package com.minarath.tv;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;

import androidx.databinding.DataBindingUtil;

import com.minarath.tv.databinding.ConfielderActivitySpleshScreenBinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class confielder_SpleshScreen extends Activity {
    public static boolean isfromplay;
    ConfielderActivitySpleshScreenBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.confielder_activity_splesh_screen);

        isfromplay = isStoreVersion(this);

        Handler handler = new Handler(Looper.myLooper());
        handler.postDelayed(() -> {
            Intent i2 = new Intent(confielder_SpleshScreen.this, confielder_Start_App.class);
            startActivity(i2);
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            finish();
        }, 2000);

        handler.postDelayed(() -> {
            binding.relLogoHolder.setVisibility(View.GONE);
        }, 1000);


    }

    public static boolean isStoreVersion(Context context) {

        boolean result = false;
        List<String> validInstallers = new ArrayList<>
                (Arrays.asList("com.android.vending", "com.google.android.feedback"));

        try {

            String installer = context.getPackageManager()
                    .getInstallerPackageName(context.getPackageName());
            result = !TextUtils.isEmpty(installer) && validInstallers.contains(installer);

        } catch (Exception e) {
        }

        return result;
    }
}
