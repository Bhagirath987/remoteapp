package com.minarath.tv;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.minarath.tv.Wifi_Connection.AdbShell;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import io.resourcepool.ssdp.client.SsdpClient;
import io.resourcepool.ssdp.model.DiscoveryListener;
import io.resourcepool.ssdp.model.SsdpRequest;
import io.resourcepool.ssdp.model.SsdpService;
import io.resourcepool.ssdp.model.SsdpServiceAnnouncement;

/* renamed from: amazon.fire.tv.stick.remote.ConnectActivity */
public class ConnectActivity extends AppCompatActivity {
    private static final String PREFS_FILE = "AdbConnectPrefs";
    private static final String TAG = "MainActivity";
    private Activity activity = this;
    SsdpClient client = SsdpClient.create();
    private Button connectButton;
    boolean doubleBackToExitPressedOnce = false;
    /* access modifiers changed from: private */
    public Adapter mAdapter;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public final ArrayList<String> myDataSet = new ArrayList<>();
    private ProgressBar spinner;

    /* renamed from: amazon.fire.tv.stick.remote.ConnectActivity$MyAdapter */
    public class MyAdapter extends Adapter<MyAdapter.ViewHolder> {
        /* access modifiers changed from: private */
        public final ArrayList<String> mDataset;

        /* renamed from: amazon.fire.tv.stick.remote.ConnectActivity$MyAdapter$ViewHolder */
        class ViewHolder extends RecyclerView.ViewHolder implements OnClickListener {
            private TextView mTextView;

            public ViewHolder(View view) {
                super(view);
                view.setOnClickListener(this);
                this.mTextView = (TextView) view.findViewById(R.id.textView);
            }

            public void setItem(String str) {
                this.mTextView.setText(str);
            }

            public void onClick(View view) {
                String str = (String) MyAdapter.this.mDataset.get(getAdapterPosition());
                ConnectActivity.this.client.stopDiscovery();
                String[] split = str.split("\n");
                String str2 = split[0];
                Intent intent = new Intent(ConnectActivity.this, AdbShell.class);
                intent.putExtra("IP", str2);
                intent.putExtra("NAME", split[split.length - 1]);
                ConnectActivity.this.startActivity(intent);
            }
        }

        MyAdapter(ArrayList<String> arrayList) {
            this.mDataset = arrayList;
        }

        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_row_item, viewGroup, false));
        }

        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            viewHolder.setItem((String) this.mDataset.get(i));
        }

        public int getItemCount() {
            return this.mDataset.size();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView((int) R.layout.activity_connect);
        setTitle("Amazon Fire TV Remote");
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }
        findViewById(R.id.recyclerView).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Toast.makeText(ConnectActivity.this.mContext, "Click Listner",
                        Toast.LENGTH_SHORT).show();
            }
        });
        this.mContext = this;
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        MyAdapter myAdapter = new MyAdapter(this.myDataSet);
        this.mAdapter = myAdapter;
        recyclerView.setAdapter(myAdapter);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        this.spinner = progressBar;
        progressBar.setVisibility(View.VISIBLE);
        SsdpRequest.discoverAll();
        this.client.discoverServices(SsdpRequest.builder().serviceType("urn:dial-multiscreen-org" +
                ":device:dial:1").build(), new DiscoveryListener() {
            public void onFailed(Exception exc) {
            }

            public void onServiceDiscovered(SsdpService ssdpService) {
                final String location = ssdpService.getLocation();
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            String str = location;
                            Log.d("deviceURL", str);
                            Document parse =
                                    DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new URL(str).openStream());
                            parse.getDocumentElement().normalize();
                            final Node item =
                                    ((Element) parse.getElementsByTagName("device").item(0)).getElementsByTagName("friendlyName").item(0).getChildNodes().item(0);
                            PrintStream printStream = System.out;
                            StringBuilder sb = new StringBuilder();
                            sb.append("Found Friendly Name: ");
                            sb.append(item.getNodeValue());
                            printStream.println(sb.toString());
                            final String[] split = location.split("/")[2].split(":");
                            PrintStream printStream2 = System.out;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("Found IP: ");
                            sb2.append(split[0]);
                            printStream2.println(sb2.toString());
                            ConnectActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    ArrayList access$100 = ConnectActivity.this.myDataSet;
                                    StringBuilder sb = new StringBuilder();
                                    sb.append(split[0]);
                                    sb.append("\n");
                                    sb.append(item.getNodeValue());
                                    access$100.add(sb.toString());
                                    ConnectActivity.this.mAdapter.notifyDataSetChanged();
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }

            public void onServiceAnnouncement(SsdpServiceAnnouncement ssdpServiceAnnouncement) {
                PrintStream printStream = System.out;
                StringBuilder sb = new StringBuilder();
                sb.append("Service announced something: ");
                sb.append(ssdpServiceAnnouncement);
                printStream.println(sb.toString());
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
