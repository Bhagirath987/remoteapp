package com.minarath.tv.confielder_htc.confielder_circontrol;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import com.minarath.tv.confielder_htc.confielder_htcircontrol.confielder_HtcIrData;

import java.util.UUID;

public class confielder_CIRControl {
    static final String CIRSERVICE_CLASS = "com.htc.cirmodule.CIRControlService";
    static final String CIRSERVICE_PATH = "com.htc.cirmodule";

    public static final String KEY_CMD_RESULT = "Result";
    public static final String KEY_COMMAND = "Command";
    public static final String KEY_DROPPABLE = "Drop";
    public static final String KEY_RESULT_ID = "RID";

    public String TAG_DEBUG = "confielder_CIRControl";
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            confielder_CIRControl.this.mService = new Messenger(iBinder);
            Log.w(confielder_CIRControl.this.TAG_DEBUG, "onServiceConnected register client");
            confielder_CIRControl.this.mIsBound = true;
            Bundle bundle = new Bundle();
            bundle.putSerializable(confielder_CIRControl.KEY_RESULT_ID, UUID.randomUUID());
            confielder_CIRControl.this.sendMessageToService(1, bundle);
            confielder_CIRControl.this.sendMessageToUI(8, null, 0, 0);
        }

        public void onServiceDisconnected(ComponentName componentName) {
            Log.w(confielder_CIRControl.this.TAG_DEBUG, "onServiceDisconnected");
            confielder_CIRControl.this.mService = null;
            confielder_CIRControl.this.mIsBound = false;
        }
    };
    private Context mContext = null;
    private Handler mHandler = null;
    public boolean mIsBound = false;
    private final Messenger mMessenger = new Messenger(new IncomingHandler());
    public Messenger mService = null;

    class IncomingHandler extends Handler {
        IncomingHandler() {
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    Log.w(confielder_CIRControl.this.TAG_DEBUG, "Got <MSG_RET_LEARN_IR>");
                    confielder_CIRControl.this.sendMessageToUI(message.what, message.getData(), message.arg1, message.arg2);
                    return;
                case 2:
                    Log.w(confielder_CIRControl.this.TAG_DEBUG, "Got <MSG_RET_TRANSMIT_IR>");
                    confielder_CIRControl.this.sendMessageToUI(message.what, message.getData(), message.arg1, message.arg2);
                    return;
                case 6:
                    Log.w(confielder_CIRControl.this.TAG_DEBUG, "Got <MSG_RET_CANCEL>");
                    confielder_CIRControl.this.sendMessageToUI(message.what, message.getData(), message.arg1, message.arg2);
                    return;
                case 7:
                    Log.w(confielder_CIRControl.this.TAG_DEBUG, "Got <MSG_RET_DISCARD>");
                    confielder_CIRControl.this.sendMessageToUI(message.what, message.getData(), message.arg1, message.arg2);
                    break;
            }
            super.handleMessage(message);
        }
    }

    public confielder_CIRControl(Context context, Handler handler) {
        if (context != null) {
            this.mContext = context;
            this.mHandler = handler;
        }
    }


    public void start() {
        if (this.mContext == null || this.mIsBound) {
            Log.e(this.TAG_DEBUG, "Cannot start because null context or is bound already!");
            return;
        }
        startCIRService();
        doBindService();
    }



    private UUID sendCommand(int i) {
        Bundle bundle = new Bundle();
        UUID randomUUID = UUID.randomUUID();
        bundle.putSerializable(KEY_RESULT_ID, randomUUID);
        String str = this.TAG_DEBUG;
        StringBuilder sb = new StringBuilder("Control(L&S): UUID=");
        sb.append(randomUUID);
        Log.w(str, sb.toString());
        String str2 = this.TAG_DEBUG;
        StringBuilder sb2 = new StringBuilder("sendCommand: ");
        sb2.append(i);
        Log.w(str2, sb2.toString());
        sendMessageToService(i, bundle);
        return randomUUID;
    }

    public UUID transmitIRCmd(confielder_HtcIrData htcIrData, boolean z) {
        UUID randomUUID;
        if (this.mContext == null || !this.mIsBound) {
            Log.e(this.TAG_DEBUG, "Cannot transmitIRCmd because null context or not bound yet!");
            return null;
        }
        synchronized (this) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(KEY_COMMAND, htcIrData);
            randomUUID = UUID.randomUUID();
            bundle.putSerializable(KEY_RESULT_ID, randomUUID);
            String str = this.TAG_DEBUG;
            StringBuilder sb = new StringBuilder("Control(L&S): UUID=");
            sb.append(randomUUID);
            Log.w(str, sb.toString());
            String str2 = this.TAG_DEBUG;
            StringBuilder sb2 = new StringBuilder("transmitIRCmd: drop=");
            sb2.append(z);
            sb2.append(" {");
            sb2.append(htcIrData.getRepeatCount());
            sb2.append(" and ");
            sb2.append(htcIrData.getFrequency());
            sb2.append(" and ");
            sb2.append(htcIrData.getFrame().length);
            sb2.append("}");
            Log.w(str2, sb2.toString());
            bundle.putBoolean(KEY_DROPPABLE, z);
            sendMessageToService(4, bundle);
        }
        return randomUUID;
    }


    private void startCIRService() {
        Intent intent = new Intent();
        intent.setClassName(CIRSERVICE_PATH, CIRSERVICE_CLASS);
        Log.w(this.TAG_DEBUG, "StartCIRService");
        this.mContext.startService(intent);
    }

    private void stopCIRService() {
        Intent intent = new Intent();
        intent.setClassName(CIRSERVICE_PATH, CIRSERVICE_CLASS);
        Log.w(this.TAG_DEBUG, "StopCIRService");
        this.mContext.stopService(intent);
    }

    public void sendMessageToService(int i, Bundle bundle) {
        if (!this.mIsBound) {
            Log.e(this.TAG_DEBUG, "sendMessageToService: mIsBound false");
        } else if (this.mService != null) {
            try {
                Message obtain = Message.obtain(null, i);
                if (bundle != null) {
                    obtain.setData(bundle);
                }
                obtain.replyTo = this.mMessenger;
                String str = this.TAG_DEBUG;
                StringBuilder sb = new StringBuilder("sendMessageToService: ");
                sb.append(i);
                Log.w(str, sb.toString());
                this.mService.send(obtain);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else {
            Log.e(this.TAG_DEBUG, "sendMessageToService: mService null");
        }
    }

    public void sendMessageToUI(int i, Bundle bundle, int i2, int i3) {
        if (this.mHandler != null) {
            Message obtain = Message.obtain(null, i, i2, i3);
            if (bundle != null) {
                obtain.setData(bundle);
            }
            String str = this.TAG_DEBUG;
            StringBuilder sb = new StringBuilder("sendMessageToUI: ");
            sb.append(i);
            sb.append(" and ");
            sb.append(i2);
            sb.append(" and ");
            sb.append(i3);
            Log.w(str, sb.toString());
            this.mHandler.sendMessage(obtain);
            return;
        }
        Log.e(this.TAG_DEBUG, "sendMessageToUI: mHandler null");
    }

    public void doBindService() {
        if (this.mContext != null) {
            Intent intent = new Intent();
            intent.setClassName(CIRSERVICE_PATH, CIRSERVICE_CLASS);
            Log.w(this.TAG_DEBUG, "doBindService");
            this.mContext.bindService(intent, this.mConnection, Context.BIND_AUTO_CREATE);
        }
    }
}
