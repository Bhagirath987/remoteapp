package com.minarath.tv.confielder_htc.confielder_htcircontrol;

import android.util.Log;
import java.io.Serializable;
import java.util.Arrays;

public class confielder_HtcIrData implements Serializable {
    public static final String TAG = "confielder_HtcIrData";
    private static int overhead = 8;
    private int[] frame;
    private int frequency;
    private int period = 150;
    private int periodTolerance = 0;
    private int repeatCount;


    public confielder_HtcIrData(int i, int i2, int[] iArr) {
        if (i > 255 || i < 1) {
            Log.e(TAG, "argument rc is invalid");
            throw new IllegalArgumentException("rc must be in range of 1 - 255");
        }
        this.repeatCount = i;
        if (i2 > 60000 || i2 < 24000) {
            Log.e(TAG, "argument freq is invalid");
            throw new IllegalArgumentException("freq must be in range of 24000 - 60000");
        }
        this.frequency = i2;
        if (iArr != null) {
            this.frame = Arrays.copyOf(iArr, iArr.length);
            if (validateFrame()) {
                calculateFramePeriod();
                Log.v(TAG, "Constructor");
                return;
            }
            Log.e(TAG, "argument array is too big or size is not even");
            throw new IllegalArgumentException("array length is too big or not even");
        }
        Log.e(TAG, "argument array is null");
        throw new IllegalArgumentException("array must be not null");
    }

    public int getRepeatCount() {
        return this.repeatCount;
    }

    public int getFrequency() {
        return this.frequency;
    }


    public int[] getFrame() {
        return Arrays.copyOf(this.frame, this.frame.length);
    }


    private void calculateFramePeriod() {
        double d = 0.0d;
        for (int i : this.frame) {
            double d2 = (double) i;
            Double.isNaN(d2);
            d += d2;
        }
        double d3 = (double) this.frequency;
        Double.isNaN(d3);
        double d4 = d / d3;
        double d5 = (double) this.repeatCount;
        Double.isNaN(d5);
        double d6 = d4 * d5 * 1000.0d;
        String str = TAG;
        StringBuilder sb = new StringBuilder("frame period = ");
        sb.append(d6);
        Log.w(str, sb.toString());
        this.period = (int) d6;
    }

    private boolean validateFrame() {
        if (this.frame.length + overhead > 1024 || this.frame.length % 2 != 0) {
            return false;
        }
        int i = 0;
        for (int i2 = 0; i2 < this.frame.length; i2++) {
            i = this.frame[i2] < 128 ? i + 1 : i + 2;
        }
        if (i + overhead > 1024) {
            return false;
        }
        return true;
    }
}
