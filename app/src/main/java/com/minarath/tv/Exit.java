package com.minarath.tv;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.minarath.tv.New.Helper.Utils;


public class Exit extends Activity {
    ImageView yes, no;
    TextView text;
    ImageView tvtitle;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exit_dialog);

        context = this;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        yes = findViewById(R.id.ivok);
        no = findViewById(R.id.ivcancel);
        text = findViewById(R.id.tvtext);
        tvtitle = findViewById(R.id.tvtitle);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();

            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
