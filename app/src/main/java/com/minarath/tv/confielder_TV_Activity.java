package com.minarath.tv;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.ConsumerIrManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.minarath.tv.New.Database.DB_Helper;
import com.minarath.tv.New.Helper.Utils;
import com.minarath.tv.New.Modal.IrCode;
import com.minarath.tv.New.Modal.TV_sharp_Codes;
import com.minarath.tv.confielder_Extra.confielder_ConsumerIrManagerCompat;
import com.minarath.tv.confielder_lge.confielder_hardware.confielder_IRBlaster.confielder_IRBlaster;
import com.minarath.tv.confielder_lge.confielder_hardware.confielder_IRBlaster.confielder_IRBlasterCallback;

import java.lang.reflect.Method;

import org.json.JSONObject;

public class confielder_TV_Activity extends Activity {
    private static Context mContext;
    ConsumerIrManager CIR;
    public boolean IRb = false;
    boolean LGIRst = false;
    boolean LGIRst2 = false;
    String adman;
    boolean b = false;
    String bv;
    int w, h;
    JSONObject currentRemote = null;
    int currentapiVersion;
    Object irS;
    confielder_ConsumerIrManagerCompat mCIR;
    private confielder_IRBlaster mIR;
    boolean mIR_ready = false;
    public confielder_IRBlasterCallback mIrBlasterReadyCallback =
            new confielder_IRBlasterCallback() {
                public void learnIRCompleted(int i) {
                }

                public void newDeviceId(int i) {
                }

                public void IRBlasterReady() {
                    Log.d("IR", "confielder_IRBlaster is really ready");
                    confielder_TV_Activity.this.mIR_ready = true;
                }
            };
    SharedPreferences preferences;
    Method sIR;
    String savedRemote;
    ImageView zero, one, two, three, four, five, six, seven, eight, nine, power, ok,
            mute, menu, exit, up_arrow, down_arrow, left_arrow, right_arrow;
    ImageView vol_plus, vol_minus, ch_plus, ch_minus, red, green, blue, yellow, back, play, stop,
            forward, backward, record, home;
    TV_sharp_Codes data;
    DB_Helper dbhelper;
    LinearLayout remoteLay, round, box_lay;
    LinearLayout bgch, bgvol;
    private String brand;

    @SuppressLint({"NewApi", "WrongConstant"})
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.confielder_act_ac_gui);

        w = getResources().getDisplayMetrics().widthPixels;
        h = getResources().getDisplayMetrics().heightPixels;

        Intent intent = getIntent();
        this.preferences = getSharedPreferences("spwremote", 0);
        this.savedRemote = intent.getStringExtra("acname");
        this.brand = intent.getStringExtra("brand");
        if(this.preferences.getBoolean("port1", false)) {
            setRequestedOrientation(1);
        }

        dbhelper = new DB_Helper(this);
        data = dbhelper.getAllProcess(brand, savedRemote);
        this.LGIRst = this.preferences.getBoolean("LGIRst", false);
        this.LGIRst2 = this.preferences.getBoolean("LGIRst2", false);
        mContext = this;
        //        loadCurrentRemote(this.savedRemote);
        this.currentapiVersion = VERSION.SDK_INT;
        this.adman = Build.MANUFACTURER;

        if("HTC".equals(this.adman) && VERSION.SDK_INT < 23) {
            try {
                this.mCIR = confielder_ConsumerIrManagerCompat
                        .createInstance(getApplicationContext());
                this.mCIR.start();
            } catch (Exception unused) {
                Log.e("IR", "Error HTC IR API 19.");
                Toast.makeText(getApplicationContext(), "Error HTC IR...Your HTC has IR blaster? " +
                        "Visit the support website.", 1).show();
            }
        }
        else if((!this.adman.equalsIgnoreCase("lge") || VERSION.SDK_INT >= 23 || this.LGIRst) &&
                !this.LGIRst2) {
            if(this.currentapiVersion >= 19) {
                this.CIR = (ConsumerIrManager) getSystemService("consumer_ir");

                if(this.CIR != null) {
                    this.IRb = this.CIR.hasIrEmitter();
                }
                if(!this.IRb) {
                    Context applicationContext = getApplicationContext();
                    StringBuilder sb = new StringBuilder();
                    sb.append("API: ");
                    sb.append(String.valueOf(this.currentapiVersion));
                    sb.append(", O.S: Android 4.4.2 or higher.\nMaybe your phone or tablet has " +
                            "not IR emitter.Visit the support website.");
                    Toast.makeText(applicationContext, sb.toString(), 0).show();
                }

            }
            this.bv = VERSION.RELEASE;
            boolean z = this.preferences.getBoolean("IRst", false);
            if((this.bv.equals("4.4.3") || this.bv.equals("4.4.4") || this.currentapiVersion >=
                    21) && !z) {
                this.b = true;
            }
        }
        else {
            this.mIR = null;
            if(confielder_IRBlaster.isSdkSupported(this)) {
                this.mIR = confielder_IRBlaster.getIRBlaster(this, this.mIrBlasterReadyCallback);
            }
            if(this.mIR == null) {
                Log.e("IR", "No IR Blaster in this device");
                Toast.makeText(getApplicationContext(), "Error LG IR...Your LG has IR blaster? " +
                        "Visit the support website.", 1).show();
                return;
            }
        }

        init();
        resize();
        Tool();
    }

    private void Tool() {
        final ImageView fav_btn = findViewById(R.id.fav_btn);
        LinearLayout tool_bar = findViewById(R.id.tool_bar);
        ImageView btn_back = findViewById(R.id.btn_back);
        TextView topbar_text = findViewById(R.id.topbar_text);
        topbar_text.setText(Utils.TV);
        Utils.setSize(mContext, fav_btn, 40, 40, false);
        Utils.setSize(mContext, btn_back, 20, 35, false);
        Utils.setSize(mContext, tool_bar, 1080, 158, false);

        if(dbhelper.isFav(savedRemote)) {
            fav_btn.setImageResource(R.drawable.fvrt_unpress);
        }
        else {
            fav_btn.setImageResource(R.drawable.fvrt_press);
        }
        fav_btn.setVisibility(View.VISIBLE);

        fav_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dbhelper.isFav(savedRemote)) {
                    Utils.delToFav(dbhelper, savedRemote, Utils.TV);
                    fav_btn.setImageResource(R.drawable.fvrt_press);
                }
                else {
                    Utils.addToFav(dbhelper, savedRemote, Utils.TV);
                    fav_btn.setImageResource(R.drawable.fvrt_unpress);
                }
            }
        });
    }

    private void init() {
        box_lay = findViewById(R.id.box_lay);
        round = findViewById(R.id.round);
        remoteLay = findViewById(R.id.remoteLay);
        zero = findViewById(R.id.zero);
        one = findViewById(R.id.one);
        two = findViewById(R.id.two);
        three = findViewById(R.id.three);
        four = findViewById(R.id.four);
        five = findViewById(R.id.five);
        six = findViewById(R.id.six);
        seven = findViewById(R.id.seven);
        eight = findViewById(R.id.eight);
        nine = findViewById(R.id.nine);
        power = findViewById(R.id.power);
        ok = findViewById(R.id.ok);
        mute = findViewById(R.id.mute);
        menu = findViewById(R.id.menu);
        exit = findViewById(R.id.exit);
        up_arrow = findViewById(R.id.up_arrow);
        down_arrow = findViewById(R.id.down_arrow);
        right_arrow = findViewById(R.id.right_arrow);
        home = findViewById(R.id.home);
        left_arrow = findViewById(R.id.left_arrow);
        vol_plus = findViewById(R.id.vol_plus);
        vol_minus = findViewById(R.id.vol_minus);
        ch_minus = findViewById(R.id.ch_minus);
        ch_plus = findViewById(R.id.ch_plus);
        red = findViewById(R.id.red);
        green = findViewById(R.id.green);
        blue = findViewById(R.id.blue);
        yellow = findViewById(R.id.yellow);
        back = findViewById(R.id.back);
        play = findViewById(R.id.play);
        stop = findViewById(R.id.stop);
        forward = findViewById(R.id.forward);
        record = findViewById(R.id.record);
        backward = findViewById(R.id.backward);
        bgch = findViewById(R.id.bgch);
        bgvol = findViewById(R.id.bgvol);
        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getZero());
            }
        });
        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getOne());
            }
        });
        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getTwo());

            }
        });
        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getThree());
            }
        });
        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getFour());

            }
        });
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getFive());
            }
        });
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getSix());
            }
        });
        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getSeven());
            }
        });
        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getEight());
            }
        });
        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getNine());
            }
        });
        power.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getPower());
            }
        });
        mute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getMute());
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getMenu());
            }
        });
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getExit());
            }
        });
        vol_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getVolume_Up());
            }
        });
        vol_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getVolume_Down());
            }
        });
        ch_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getChannel_Up());
            }
        });
        ch_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getChannel_Down());
            }
        });
        up_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getNavigate_Up());
            }
        });
        down_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getNavigate_Down());
            }
        });
        right_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getNavigate_Right());
            }
        });
        left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getNavigate_Left());
            }
        });
        red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getRed());
            }
        });
        green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getGreen());
            }
        });
        yellow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getYellow());
            }
        });
        blue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getBlue());
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getBack());
            }
        });
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getPlay());
            }
        });
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getStop());
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getSelect());
            }
        });
        backward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //                TODO
                //                SendIRFunction(data.getSelect());
                //                SendIRFunction("skipforward");
            }
        });
        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //                TODO
                //                SendIRFunction(data.getSelect());
                //                SendIRFunction("record");
            }
        });
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendIRFunction(data.getFast_Forward());
                //                SendIRFunction("fastforward");
            }
        });


    }


    private void resize() {
        Utils.setSize(confielder_TV_Activity.this, zero, 137, 137, false);
        Utils.setSize(confielder_TV_Activity.this, one, 137, 137, false);
        Utils.setSize(confielder_TV_Activity.this, two, 137, 137, false);
        Utils.setSize(confielder_TV_Activity.this, three, 137, 137, false);
        Utils.setSize(confielder_TV_Activity.this, four, 137, 137, false);
        Utils.setSize(confielder_TV_Activity.this, five, 137, 137, false);
        Utils.setSize(confielder_TV_Activity.this, six, 137, 137, false);
        Utils.setSize(confielder_TV_Activity.this, seven, 137, 137, false);
        Utils.setSize(confielder_TV_Activity.this, eight, 137, 137, false);
        Utils.setSize(confielder_TV_Activity.this, nine, 137, 137, false);


        Utils.setSize(confielder_TV_Activity.this, power, 150, 150, false);
        Utils.setSize(confielder_TV_Activity.this, ok, 216, 216, false);
        Utils.setSize(confielder_TV_Activity.this, mute, 150, 150, false);

        Utils.setSize(confielder_TV_Activity.this, home, 137, 137, false);
        Utils.setSize(confielder_TV_Activity.this, menu, 137, 137, false);

        Utils.setSize(confielder_TV_Activity.this, exit, 141, 141, false);
        Utils.setSize(confielder_TV_Activity.this, up_arrow, 52, 52, false);
        Utils.setSize(confielder_TV_Activity.this, down_arrow, 52, 52, false);
        Utils.setSize(confielder_TV_Activity.this, right_arrow, 52, 52, false);
        Utils.setSize(confielder_TV_Activity.this, left_arrow, 52, 52, false);

        Utils.setSize(confielder_TV_Activity.this, vol_plus, 124, 123, false);
        Utils.setSize(confielder_TV_Activity.this, vol_minus, 124, 123, false);
        Utils.setSize(confielder_TV_Activity.this, ch_minus, 124, 123, false);
        Utils.setSize(confielder_TV_Activity.this, ch_plus, 124, 123, false);

        Utils.setSize(confielder_TV_Activity.this, red, 127, 127, false);
        Utils.setSize(confielder_TV_Activity.this, green, 127, 127, false);
        Utils.setSize(confielder_TV_Activity.this, blue, 127, 127, false);
        Utils.setSize(confielder_TV_Activity.this, yellow, 127, 127, false);

        Utils.setSize(confielder_TV_Activity.this, back, 162, 161, false);
        Utils.setSize(confielder_TV_Activity.this, play, 141, 141, false);
        Utils.setSize(confielder_TV_Activity.this, stop, 141, 141, false);
        Utils.setSize(confielder_TV_Activity.this, forward, 141, 141, false);
        Utils.setSize(confielder_TV_Activity.this, record, 141, 141, false);
        Utils.setSize(confielder_TV_Activity.this, backward, 141, 141, false);
        Utils.setSize(confielder_TV_Activity.this, remoteLay, 862, 2599, false);
        Utils.setSize(confielder_TV_Activity.this, round, 507, 507, false);
        Utils.setSize(confielder_TV_Activity.this, bgch, 152, 415, false);
        Utils.setSize(confielder_TV_Activity.this, bgvol, 152, 415, false);

    }

    public void GoBack(View view) {
        onBackPressed();
    }


    public void SendIRFunction(IrCode str) {
        try {
            sendIRCode(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint({"WrongConstant", "NewApi"})
    public void sendIRCode(IrCode str) {
        //        if (this.preferences.getBoolean("vib", true)) {
        //            ((Vibrator) getSystemService("vibrator")).vibrate(40);
        //        }
        //        if (str.startsWith("0000 "))
        //        {
        //            str=convertProntoHexStringToIntString(str);
        //        }
        if("HTC".equals(this.adman) && VERSION.SDK_INT < 23) {
            try {
                //                String[] split = str.split(",");
                //                int parseInt = Integer.parseInt(split[0]);
                //                int[] iArr = new int[(split.length - 1)];
                //                int i = 0;
                //                while (i < split.length - 1) {
                //                    int i2 = i + 1;
                //                    iArr[i] = Integer.parseInt(split[i2]);
                //                    i = i2;
                //                }

                String responce = str.getIrCode().replaceAll(" ", "");
                String[] split1 = responce.split(",");

                int[] data4 = convert(split1);

                this.mCIR.transmit(Integer.parseInt(str.getFrequency()), data4);
                //                Log.e("INT", "=>" + parseInt);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(" IR ", "Error HTC IR transmitting.");
                Toast.makeText(getApplicationContext(), "Error HTC IR transmitting.Your HTC has " +
                        "IR blaster ?", 0).show();
            }
        }
        else if((this.adman.equalsIgnoreCase("lge") && VERSION.SDK_INT < 23 && !this.LGIRst) ||
                this.LGIRst2) {
            try {
                //                String[] split2 = str.split(",");
                //                int parseInt2 = Integer.parseInt(split2[0]);
                //                int[] iArr2 = new int[(split2.length - 1)];
                //                int i3 = 0;
                //                while(i3 < split2.length - 1) {
                //                    int i4 = i3 + 1;
                //                    iArr2[i3] = Integer.parseInt(split2[i4]);
                //                    i3 = i4;
                //                }
                //                int length = iArr2.length;
                //                for(int i5 = 0; i5 < length; i5++) {
                //                    iArr2[i5] = (int) ((((float) iArr2[i5]) * 1000000.0f) / (
                //                    (float) parseInt2));
                //                }
                //                Log.e("INT", "=>" + parseInt2);

                String responce = str.getIrCode().replaceAll(" ", "");
                String[] split1 = responce.split(",");

                int[] data4 = convert(split1);

                this.mIR.sendIRPattern(Integer.parseInt(str.getFrequency()), data4);

                //                this.mIR.sendIRPattern(parseInt2, iArr2);
            } catch (Exception e2) {
                e2.printStackTrace();
                Log.e(" IR ", "Error LGE IR transmitting.");
                Toast.makeText(getApplicationContext(), "Error LG IR transmitting...", 0).show();
            }
        }
        else if(this.currentapiVersion < 19 || !this.IRb) {
            try {
                this.irS = getSystemService("irda");
                this.irS.getClass();
                this.sIR = this.irS.getClass().getMethod("write_irsend", new Class[]{String.class});
                this.sIR.invoke(this.irS, new Object[]{str});
            } catch (Exception e3) {
                e3.printStackTrace();
                Log.e("Samsung IR ", "Error IR transmitting...");
                Toast.makeText(getApplicationContext(), "Error transmitting IR signal... Your " +
                        "phone or tablet has IR ?", 1).show();
            }
        }
        else {
            try {
                //                str="38000,343,172";
                //                String[] split3 = str.split(",");
                //                int parseInt3 = Integer.parseInt(split3[0]);
                //                int[] iArr3 = new int[(split3.length - 1)];
                //                int i6 = 0;
                //                while(i6 < split3.length - 1) {
                //                    int i7 = i6 + 1;
                //                    iArr3[i6] = Integer.parseInt(split3[i7]);
                //                    i6 = i7;
                //                }
                //                if(this.b) {
                //                    int length2 = iArr3.length;
                //                    for(int i8 = 0; i8 < length2; i8++) {
                //                        iArr3[i8] = (int) ((((float) iArr3[i8]) * 1000000.0f) /
                //                        ((float)
                //                                parseInt3));
                //                    }
                //                }
                //                Log.e("INT", "=>" + iArr3);

                String responce = str.getIrCode().replaceAll(" ", "");
                String[] split1 = responce.split(",");

                int[] data4 = convert(split1);

                this.CIR.transmit(Integer.parseInt(str.getFrequency()), data4);

                //                this.CIR.transmit(parseInt3, iArr3);
            } catch (Exception e4) {
                e4.printStackTrace();
                Log.e(" IR ", "Error transmitting." + e4.getMessage());
                Toast.makeText(getApplicationContext(), "Error IR transmitting.", 1).show();
            }
        }
    }

    private int[] convert(String[] string) {
        int number[] = new int[string.length];

        for(int i = 0; i < string.length; i++) {
            number[i] = Integer.parseInt(string[i]);
        }
        return number;
    }

    public static Context getContext() {
        return mContext;
    }

    public String convertProntoHexStringToIntString(String s) {
        String[] codes = s.split(" ");
        StringBuilder sb = new StringBuilder();
        sb.append(new StringBuilder(String.valueOf(getFrequency(codes[1]))).append(",").toString());
        for(int i = 4; i < codes.length; i++) {
            sb.append(new StringBuilder(String.valueOf(Integer.parseInt(codes[i], 16))).append(","
            ).toString());
        }
        return sb.toString();
    }

    public static String getFrequency(String s) {
        return Integer.valueOf((int) (1000000.0d / (((double) Integer.parseInt(s, 16)) * 0.241246d))).toString();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
