package com.minarath.tv;

import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.JSONException;
import org.json.JSONObject;

public class confielder_Utils {
    public static void showMsgBox(String str, String str2) {
    }

    public static JSONObject getJSONObjectFromFile(String str, String str2) {

        Log.e("No","confielder_Utils:"+str2+"/"+str);
        try {
            return new JSONObject(getJSONStringFromFile(str, str2));
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("Error","confielder_Utils:"+e.getMessage());
            return null;
        }
    }

    public static String getJSONStringFromFile(String str, String str2) {
        InputStream inputStream;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(Environment.getExternalStorageDirectory().getAbsolutePath());
            sb.append("/");
            sb.append(str2);
            sb.append(str.replace(" ", "_"));
            File file = new File(sb.toString());
            if (file.exists()) {
                inputStream = new FileInputStream(file);
            } else {
                AssetManager assets = confielder_TV_Activity.getContext().getAssets();
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str2);
                sb2.append(str.replace(" ", "_"));
                inputStream = assets.open(sb2.toString());
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb3 = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb3.append(readLine);
                } else {
                    bufferedReader.close();
                    inputStream.close();
                    return sb3.toString();
                }
            }
        } catch (Exception e) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Error reading file, check JSON format\n");
            sb4.append(str);
            showMsgBox("Error reading file", sb4.toString());
            e.printStackTrace();
            return null;
        }
    }

//    public static ArrayList<String> lst(String str) {
//        ArrayList<String> arrayList = new ArrayList<>();
//        try {
//            String[] list = confielder_Main2Activity.getContext().getAssets().list(str);
//            for (String replace : list) {
//                arrayList.add(replace.replace("_", " "));
//            }
//            String absolutePath = Environment.getExternalStorageDirectory().getAbsolutePath();
//            StringBuilder sb = new StringBuilder();
//            sb.append(absolutePath);
//            sb.append("/");
//            sb.append(str);
//            File file = new File(sb.toString());
//            if (file.exists()) {
//                String[] list2 = file.list();
//                if (list2 != null) {
//                    for (String replace2 : list2) {
//                        arrayList.add(replace2.replace("_", " "));
//                    }
//                }
//            }
//
//
//            Collections.sort(arrayList);
//            return arrayList;
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.e("error","==>>"+e.getMessage());
//            return null;
//        }
//    }


    public static String getFrequency(String str) {
        double parseInt = (double) Integer.parseInt(str, 16);
        Double.isNaN(parseInt);
        return Integer.valueOf((int) (1000000.0d / (parseInt * 0.241246d))).toString();
    }
}
