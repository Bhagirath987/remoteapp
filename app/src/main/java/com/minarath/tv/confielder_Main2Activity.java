package com.minarath.tv;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.minarath.tv.New.Database.DB_Helper;
import com.minarath.tv.New.Modal.RemoteCodes;
import com.minarath.tv.New.OnItemSelectionListener;
import com.minarath.tv.New.SubList_Adapter;
import com.minarath.tv.utils.GridSpacingItemDecoration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class confielder_Main2Activity extends Activity {

    private static Context context;
    RecyclerView consol;
    ArrayList<String> list = new ArrayList<>();
    ImageView btn_back;
    int w, h;
    private DB_Helper db_helper;
    private String brand;

    public static Context getContext() {
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.confielder_activity_main2);
        w = getResources().getDisplayMetrics().widthPixels;
        h = getResources().getDisplayMetrics().heightPixels;
        context = this;
        brand = getIntent().getStringExtra("brand");
        db_helper = new DB_Helper(this);
        new Copy_database().execute();
        consol = findViewById(R.id.consol);
         consol.setHasFixedSize(true);
        consol.setLayoutManager(new GridLayoutManager(this, 2));
        int spanCount = 2; // 3 columns
        int spacing = 30; // 50px
        boolean includeEdge = true;
        consol.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
    }

    class Copy_database extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            File database = confielder_Main2Activity.this.getDatabasePath(DB_Helper.DBNAME);

            if (false == database.exists()) {
                db_helper.getReadableDatabase();
                if (db_helper.copyDataBase(confielder_Main2Activity.this)) {
                    Log.e("Database Status", "Database Copied");
                } else {
                    Log.e("Database Status", "Database Copy error");
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            fillList();
            consol.setAdapter(new confielder_Recycler_Adapter(confielder_Main2Activity.this, list,
                    new OnItemSelectionListener() {
                        @Override
                        public void setPos(int pos) {
                        }

                        @Override
                        public void setSelected(String isSelected) {

                            Intent intent;
                            intent = new Intent(context, confielder_TV_Activity.class);
                            intent.putExtra("acname", isSelected);
                            intent.putExtra("brand", brand);
                            startActivity(intent);
                        }


                    }));
        }
    }

    private void fillList() {
        List<RemoteCodes> list1 = db_helper.getCode(brand, "TV");
        for (int i = 0; i < list1.size(); i++) {
            if (!list.contains(list1.get(i).getFragment())) {
                list.add(list1.get(i).getFragment());
            }
        }
        Collections.sort(list);
    }

    public void GoBack(View view) {
        finish();
    }

}