package com.minarath.tv;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.minarath.tv.New.Activity.Device_List_Activity;
import com.minarath.tv.New.Helper.Utils;
import com.minarath.tv.New.OnItemSelectionListener;

public class confielder_Recycler_Adapter extends RecyclerView.Adapter<confielder_Recycler_ViewHolder> {
    OnItemSelectionListener listener;
    private Context context;
    ArrayList<String> list = new ArrayList<>();

    public confielder_Recycler_Adapter(Context context, ArrayList<String> list,
                                       OnItemSelectionListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public confielder_Recycler_ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                             int viewType) {
        return new confielder_Recycler_ViewHolder(LayoutInflater.from(context).inflate(R.layout.confielder_item,
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull confielder_Recycler_ViewHolder holder,
                                 final int position) {

        int w = context.getResources().getDisplayMetrics().widthPixels;
        int h = context.getResources().getDisplayMetrics().heightPixels;

        if(context instanceof Device_List_Activity) {
            holder.ic_item.setText(list.get(position));
        }
        holder.ic_item.setVisibility(View.VISIBLE);
        holder.ic_item.setText(list.get(position));
        holder.crdMain.setOnClickListener(view -> listener.setSelected(list.get(position)));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(ArrayList<String> list) {
        this.list = list;
        notifyDataSetChanged();
    }
}

class confielder_Recycler_ViewHolder extends RecyclerView.ViewHolder {
    TextView ic_item;
    ConstraintLayout R_layout;
    CardView crdMain;

    public confielder_Recycler_ViewHolder(@NonNull View itemView) {
        super(itemView);
        ic_item = itemView.findViewById(R.id.ic_item);
        R_layout = itemView.findViewById(R.id.R_layout);
        crdMain = itemView.findViewById(R.id.crdMain);
    }
}