package com.minarath.tv.Wifi_Connection;

import android.util.Base64;

import com.tananaev.adblib.AdbBase64;

/* renamed from: amazon.fire.tv.stick.remote.AndroidBase64 */
public class AndroidBase64 implements AdbBase64 {
    public String encodeToString(byte[] bArr) {
        return Base64.encodeToString(bArr, 2);
    }
}
