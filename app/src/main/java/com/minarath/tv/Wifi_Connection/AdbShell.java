package com.minarath.tv.Wifi_Connection;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;

import com.tananaev.adblib.AdbConnection;
import com.tananaev.adblib.AdbCrypto;
import com.minarath.tv.ConnectActivity;
import com.minarath.tv.R;

import java.io.IOException;
import java.net.Socket;

import androidx.appcompat.app.AppCompatActivity;

/* renamed from: amazon.fire.tv.stick.remote.AdbShell */
public class AdbShell extends AppCompatActivity {
    /* access modifiers changed from: private */

    /* renamed from: IP */
    public String f0IP;
    private String NAME;
    /* access modifiers changed from: private */
    public String TAG = "AdbShell";
    /* access modifiers changed from: private */
    public AdbConnection connection;

    /* renamed from: amazon.fire.tv.stick.remote.AdbShell$AsyncTaskRunner */
    private class AsyncTaskRunner extends AsyncTask<String, String, String> {
        private AsyncTaskRunner() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... strArr) {
            try {
                if (strArr[2] == "true") {
                    Socket socket = new Socket(strArr[0], 5555);
                    AdbCrypto readCryptoConfig = AdbUtils.readCryptoConfig(AdbShell.this.getFilesDir());
                    if (readCryptoConfig == null) {
                        readCryptoConfig =
                                AdbUtils.writeNewCryptoConfig(AdbShell.this.getFilesDir());
                    }
                    AdbShell.this.connection = AdbConnection.create(socket, readCryptoConfig);
                    AdbShell.this.connection.connect();
                    AdbShell.this.connection.open("shell:logcat");
                    System.out.println(AdbShell.this.connection.toString());
                    Log.d(AdbShell.this.TAG, AdbShell.this.connection.toString());
                } else {
                    System.out.println(AdbShell.this.connection.toString());
                    AdbConnection access$200 = AdbShell.this.connection;
                    StringBuilder sb = new StringBuilder();
                    sb.append("shell:");
                    sb.append(strArr[1]);
                    access$200.open(sb.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Button imageButton;
        super.onCreate(bundle);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView((int) R.layout.activity_adb_shell);
        this.f0IP = getIntent().getStringExtra("IP");
        this.NAME = getIntent().getStringExtra("NAME");
        StringBuilder sb = new StringBuilder();
        sb.append("Connected - ");
        sb.append(this.NAME);
        setTitle(sb.toString());
        SharedPreferences sharedPreferences = getSharedPreferences("MyPrefs", 0);
        sharedPreferences.edit();
        String string = sharedPreferences.getString("TimeStamp", "");
        StringBuilder sb2 = new StringBuilder();
        sb2.append("DDDD=");
        sb2.append(string);
        Log.d("TAG", sb2.toString());
        Button imageButton2 = findViewById(R.id.imageButtonUP);
        Button imageButton3 = findViewById(R.id.imageButtonDOWN);
        Button imageButton4 = findViewById(R.id.imageButtonLEFT);
        Button imageButton5 = findViewById(R.id.imageButtonRIGHT);
        Button imageButton6 = findViewById(R.id.imageButtonCENTER);
        Button imageButton7 = findViewById(R.id.imageButtonBACK);
        Button imageButton8 = findViewById(R.id.imageButtonHOME);
        Button imageButton9 = findViewById(R.id.imageButtonMENU);
        Button imageButton10 = findViewById(R.id.imageButtonREWIND);
        Button imageButton11 = findViewById(R.id.imageButtonFASTFORWARD);
        Button imageButton12 = findViewById(R.id.imageButtonPLAYPAUSE);
        final Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        try {
            AsyncTaskRunner asyncTaskRunner = new AsyncTaskRunner();
            String[] strArr = new String[3];
            imageButton = imageButton12;
            try {
                strArr[0] = this.f0IP;
                strArr[1] = "connection";
                strArr[2] = "true";
                asyncTaskRunner.execute(strArr);
            } catch (Exception e) {
                e = e;
            }
        } catch (Exception e2) {
            imageButton = imageButton12;
            e2.printStackTrace();
            imageButton2.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    vibrator.vibrate(10);
                    String str = "input keyevent 19";
                    try {
                        new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str,
                                "false"});
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            imageButton3.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    vibrator.vibrate(10);
                    String str = "input keyevent 20";
                    try {
                        new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str,
                                "false"});
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            imageButton4.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    vibrator.vibrate(10);
                    String str = "input keyevent 21";
                    try {
                        new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str,
                                "false"});
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            imageButton5.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    vibrator.vibrate(10);
                    String str = "input keyevent 22";
                    try {
                        new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str, "false"});
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            imageButton6.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    vibrator.vibrate(10);
                    String str = "input keyevent 23";
                    try {
                        new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str, "false"});
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            imageButton7.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    vibrator.vibrate(10);
                    String str = "input keyevent 4";
                    try {
                        new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str, "false"});
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            imageButton8.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    vibrator.vibrate(10);
                    String str = "input keyevent 3";
                    try {
                        new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str,
                                "false"});
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            imageButton9.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    vibrator.vibrate(10);
                    String str = "input keyevent 82";
                    try {
                        new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str,
                                "false"});
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            imageButton10.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    vibrator.vibrate(10);
                    String str = "input keyevent 89";
                    try {
                        new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str,
                                "false"});
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            imageButton11.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    vibrator.vibrate(10);
                    String str = "input keyevent 90";
                    try {
                        new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str,
                                "false"});
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            imageButton.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    vibrator.vibrate(10);
                    String str = "input keyevent 85";
                    try {
                        new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str,
                                "false"});
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        imageButton2.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                vibrator.vibrate(10);
                String str = "input keyevent 19";
                try {
                    new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str, "false"});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        imageButton3.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                vibrator.vibrate(10);
                String str = "input keyevent 20";
                try {
                    new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str, "false"});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        imageButton4.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                vibrator.vibrate(10);
                String str = "input keyevent 21";
                try {
                    new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str, "false"});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        imageButton5.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                vibrator.vibrate(10);
                String str = "input keyevent 22";
                try {
                    new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str, "false"});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        imageButton6.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                vibrator.vibrate(10);
                String str = "input keyevent 23";
                try {
                    new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str, "false"});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        imageButton7.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                vibrator.vibrate(10);
                String str = "input keyevent 4";
                try {
                    new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str, "false"});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        imageButton8.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                vibrator.vibrate(10);
                String str = "input keyevent 3";
                try {
                    new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str, "false"});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        imageButton9.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                vibrator.vibrate(10);
                String str = "input keyevent 82";
                try {
                    new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str, "false"});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        imageButton10.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                vibrator.vibrate(10);
                String str = "input keyevent 89";
                try {
                    new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str, "false"});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        imageButton11.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                vibrator.vibrate(10);
                String str = "input keyevent 90";
                try {
                    new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str, "false"});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        imageButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                vibrator.vibrate(10);
                String str = "input keyevent 85";
                try {
                    new AsyncTaskRunner().execute(new String[]{AdbShell.this.f0IP, str, "false"});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void onBackPressed() {
        try {
            this.connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int parseInt = Integer.parseInt(Long.valueOf(System.currentTimeMillis() / 1000).toString());
        SharedPreferences sharedPreferences = getSharedPreferences("MyPrefs", 0);
        Editor edit = sharedPreferences.edit();
        String str = "TimeStamp";
        String string = sharedPreferences.getString(str, "");
        String str2 = "TAG";
        if (parseInt > Integer.parseInt(string) + Global.TSCAM) {

        }
        StringBuilder sb = new StringBuilder();
        sb.append("dddd");
        sb.append(parseInt);
        sb.append(" ");
        sb.append(Integer.parseInt(string) + Global.TSCAM);
        Log.d(str2, sb.toString());
        startActivity(new Intent(this, ConnectActivity.class));
        super.onBackPressed();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }
}
