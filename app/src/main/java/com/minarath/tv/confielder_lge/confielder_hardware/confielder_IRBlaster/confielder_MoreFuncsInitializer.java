package com.minarath.tv.confielder_lge.confielder_hardware.confielder_IRBlaster;

import android.util.Log;
import com.minarath.tv.confielder_uei.confielder_encryption.confielder_algorithm.confielder_Scrambler;
import com.minarath.tv.confielder_uei.confielder_encryption.confielder_helpers.confielder_CallerHelper;
import java.io.IOException;
import java.io.InputStream;

class confielder_MoreFuncsInitializer {
    private static final String TAG = "Initializer";
    private static boolean mLOG = false;
    private byte[] mBuffer;
    private confielder_CallerHelper mHelper;
    IOException e;

    public confielder_MoreFuncsInitializer() {
        this.mHelper = null;
        this.mBuffer = null;
        this.mHelper = new confielder_CallerHelper();
        InputStream readToken = readToken();
        if (readToken != null) {
            parseToken(readToken);
        } else if (mLOG) {
            Log.d(TAG, "Problem with resources to initialize additional functionalities (e.g. learning) of IR blaster.");
        }
    }

    public confielder_MoreFuncsInitializer(InputStream inputStream) {
        this.mHelper = null;
        this.mBuffer = null;
        this.mHelper = new confielder_CallerHelper();
        if (inputStream == null) {
            inputStream = readToken();
        }
        if (inputStream != null) {
            parseToken(inputStream);
        } else if (mLOG) {
            Log.d(TAG, "Problem with resources to initialize additional functionalities (e.g. learning) of IR blaster.");
        }
    }

    public String getMoreFuncsToken() {
        if (this.mBuffer != null) {
            return this.mHelper.getEncryptedStringBase64(this.mBuffer);
        }
        return null;
    }


    private void parseToken(InputStream inputStream) {
        byte[] bArr;
        byte[] bArr2;
        if (mLOG) {
            Log.d(TAG, "parseToken start");
        }
        try {
            bArr2 = new byte[inputStream.available()];
            try {
                inputStream.read(bArr2);
                inputStream.close();
                bArr = confielder_Scrambler.vencr(bArr2);
            } catch (IOException e) {
                e = e;
                e.printStackTrace();
                if (mLOG) {
                }
                bArr = bArr2;
                if (bArr == null) {
                }
                if (mLOG) {
                }
                if (mLOG) {
                }
            }
        } catch (IOException e2) {
            e = e2;
            bArr2 = null;
            e.printStackTrace();
            if (mLOG) {
                Log.d(TAG, "parseToken fail");
            }
            bArr = bArr2;
            if (bArr == null) {
            }
            if (mLOG) {
            }
            if (mLOG) {
            }
        }
        if (bArr == null && bArr.length > 0) {
            this.mBuffer = bArr;
        } else if (mLOG) {
            Log.w(TAG, "Token of additional functionalities had initialization problem.");
        }
        if (mLOG) {
            Log.d(TAG, "parseToken OK");
        }
    }

    public InputStream readToken() {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        if (contextClassLoader == null) {
            contextClassLoader = Class.class.getClassLoader();
        }
        return contextClassLoader.getResourceAsStream("assets/util");
    }
}
