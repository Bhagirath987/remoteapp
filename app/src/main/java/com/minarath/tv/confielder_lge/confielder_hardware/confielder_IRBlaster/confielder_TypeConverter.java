package com.minarath.tv.confielder_lge.confielder_hardware.confielder_IRBlaster;

import com.minarath.tv.confielder_uei.confielder_control.confielder_Device;
import com.minarath.tv.confielder_uei.confielder_control.confielder_IRAction;
import com.minarath.tv.confielder_uei.confielder_control.confielder_IRFunction;
import com.minarath.tv.confielder_uei.confielder_control.confielder_LearnedIRData;
import java.util.ArrayList;
import java.util.List;

class confielder_TypeConverter {
    confielder_TypeConverter() {
    }

    public static confielder_Device convertDeviceToInternal(confielder_Device device) {
        if (device == null) {
            return null;
        }
        confielder_Device device2 = new confielder_Device();
        device2.Name = device.Name;
        device2.Brand = device.Brand;
        device2.Model = device.Model;
        device2.DeviceTypeName = device.DeviceTypeName;
        device2.Id = device.Id;
        device2.Functions = device.Functions;
        device2.Tag = device.Tag;
        device2.KeyFunctions = convertIrFunctionListToInternal(device.KeyFunctions);
        return device2;
    }

    public static confielder_Device convertDeviceFromInternal(confielder_Device device) {
        if (device == null) {
            return null;
        }
        confielder_Device device2 = new confielder_Device(device.Name, device.Brand, device.Model, device.DeviceTypeName, device.Id, device.Functions, device.Tag, convertIrFunctionListFromInternal(device.KeyFunctions));
        return device2;
    }

    public static confielder_Device[] convertDeviceArrayToInternal(confielder_Device[] deviceArr) {
        if (deviceArr == null || deviceArr.length <= 0) {
            return null;
        }
        confielder_Device[] deviceArr2 = new confielder_Device[deviceArr.length];
        for (int i = 0; i < deviceArr.length; i++) {
            deviceArr2[i] = convertDeviceToInternal(deviceArr[i]);
        }
        return deviceArr2;
    }

    public static confielder_Device[] convertDeviceArrayFromInternal(confielder_Device[] deviceArr) {
        if (deviceArr == null || deviceArr.length <= 0) {
            return null;
        }
        confielder_Device[] deviceArr2 = new confielder_Device[deviceArr.length];
        for (int i = 0; i < deviceArr.length; i++) {
            deviceArr2[i] = convertDeviceFromInternal(deviceArr[i]);
        }
        return deviceArr2;
    }

    public static confielder_IRFunction convertIrFunctionToInternal(confielder_IRFunction iRFunction) {
        if (iRFunction == null) {
            return null;
        }
        confielder_IRFunction iRFunction2 = new confielder_IRFunction();
        iRFunction2.Id = iRFunction.Id;
        iRFunction2.IsLearned = iRFunction.IsLearned;
        iRFunction2.LearnedCode = iRFunction.LearnedCode;
        iRFunction2.Name = iRFunction.Name;
        return iRFunction2;
    }

    public static confielder_IRFunction convertIrFunctionFromInternal(confielder_IRFunction iRFunction) {
        if (iRFunction != null) {
            return new confielder_IRFunction(iRFunction.Name, iRFunction.Id, iRFunction.IsLearned, iRFunction.LearnedCode);
        }
        return null;
    }

    public static List<confielder_IRFunction> convertIrFunctionListToInternal(List<confielder_IRFunction> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (confielder_IRFunction convertIrFunctionToInternal : list) {
            arrayList.add(convertIrFunctionToInternal(convertIrFunctionToInternal));
        }
        return arrayList;
    }

    public static List<confielder_IRFunction> convertIrFunctionListFromInternal(List<confielder_IRFunction> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (confielder_IRFunction convertIrFunctionFromInternal : list) {
            arrayList.add(convertIrFunctionFromInternal(convertIrFunctionFromInternal));
        }
        return arrayList;
    }

    public static confielder_IRAction convertIrActionToInternal(confielder_IRAction iRAction) {
        if (iRAction != null) {
            return new confielder_IRAction(iRAction.DeviceId, iRAction.Function, iRAction.Duration);
        }
        return null;
    }

    public static confielder_IRAction convertIrActionFromInternal(confielder_IRAction iRAction) {
        if (iRAction != null) {
            return new confielder_IRAction(iRAction.DeviceId, iRAction.Function, iRAction.Duration);
        }
        return null;
    }

    public static List<confielder_IRAction> convertIrActionListToInternal(List<confielder_IRAction> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (confielder_IRAction convertIrActionToInternal : list) {
            arrayList.add(convertIrActionToInternal(convertIrActionToInternal));
        }
        return arrayList;
    }

    public static List<confielder_IRAction> convertIrActionListFromInternal(List<confielder_IRAction> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (confielder_IRAction convertIrActionFromInternal : list) {
            arrayList.add(convertIrActionFromInternal(convertIrActionFromInternal));
        }
        return arrayList;
    }

    public static confielder_LearnedIRData convertLearnedIrDataToInternal(confielder_LearnedIrData learnedIrData) {
        if (learnedIrData != null) {
            return new confielder_LearnedIRData(learnedIrData.Id, learnedIrData.Data);
        }
        return null;
    }

    public static confielder_LearnedIrData convertLearnedIrDataFromInternal(confielder_LearnedIRData learnedIRData) {
        if (learnedIRData != null) {
            return new confielder_LearnedIrData(learnedIRData.Id, learnedIRData.Data);
        }
        return null;
    }

    public static confielder_LearnedIRData[] convertLearnedIrDataArrayToInternal(confielder_LearnedIrData[] learnedIrDataArr) {
        if (learnedIrDataArr == null || learnedIrDataArr.length <= 0) {
            return null;
        }
        confielder_LearnedIRData[] learnedIRDataArr = new confielder_LearnedIRData[learnedIrDataArr.length];
        for (int i = 0; i < learnedIrDataArr.length; i++) {
            learnedIRDataArr[i] = convertLearnedIrDataToInternal(learnedIrDataArr[i]);
        }
        return learnedIRDataArr;
    }

    public static confielder_LearnedIrData[] convertLearnedIrDataArrayFromInternal(confielder_LearnedIRData[] learnedIRDataArr) {
        if (learnedIRDataArr == null || learnedIRDataArr.length <= 0) {
            return null;
        }
        confielder_LearnedIrData[] learnedIrDataArr = new confielder_LearnedIrData[learnedIRDataArr.length];
        for (int i = 0; i < learnedIRDataArr.length; i++) {
            learnedIrDataArr[i] = convertLearnedIrDataFromInternal(learnedIRDataArr[i]);
        }
        return learnedIrDataArr;
    }
}
