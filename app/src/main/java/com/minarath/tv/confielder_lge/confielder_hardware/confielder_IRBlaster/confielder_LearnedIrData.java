package com.minarath.tv.confielder_lge.confielder_hardware.confielder_IRBlaster;

public class confielder_LearnedIrData {
    public byte[] Data = null;
    public int Id = 0;

    public confielder_LearnedIrData(int i, byte[] bArr) {
        this.Id = i;
        this.Data = bArr;
        if (this.Data == null) {
            this.Data = new byte[0];
        }
    }
}
