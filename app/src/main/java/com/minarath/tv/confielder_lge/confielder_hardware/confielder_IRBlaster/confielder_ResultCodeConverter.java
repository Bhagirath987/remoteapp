package com.minarath.tv.confielder_lge.confielder_hardware.confielder_IRBlaster;

class confielder_ResultCodeConverter {
    public static int matchToExternalResultCode(int i) {
        switch (i) {
            case -2:
                return 1;
            case -1:
                return 1;
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return 1;
            case 3:
                return 1;
            case 4:
                return 1;
            case 5:
                return 1;
            case 6:
                return 1;
            case 7:
                return 7;
            case 8:
                return 8;
            case 9:
                return 9;
            case 10:
                return 10;
            case 11:
                return 11;
            case 12:
                return 12;
            case 13:
                return 13;
            case 14:
                return 14;
            case 15:
                return 15;
            case 16:
                return 1;
            case 17:
                return 1;
            case 18:
                return 1;
            case 19:
                return 1;
            case 20:
                return 1;
            case 21:
                return 1;
            default:
                return 1;
        }
    }

    confielder_ResultCodeConverter() {
    }
}
