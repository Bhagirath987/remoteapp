package com.minarath.tv.confielder_lge.confielder_hardware.confielder_IRBlaster;

public class confielder_ResultCode {

    public static String getString(int i) {
        if (i == 17) {
            return "Cannot delete this device.";
        }
        switch (i) {
            case 0:
                return "Success.";
            case 1:
                return "Error.";
            default:
                switch (i) {
                    case 7:
                        return "Out of Memory in confielder_IRBlaster.";
                    case 8:
                        return "Maximum devices allowed has reached.";
                    case 9:
                        return "SDK services are not ready during the initialization process.";
                    case 10:
                        return "Invalid Authentication Key.";
                    case 11:
                        return "Error occurred while opening database.";
                    case 12:
                        return "IR learning is failed.";
                    case 13:
                        return "IR learning is timed out.";
                    case 14:
                        return "IR learning is aborted.";
                    case 15:
                        return "Maximum learned functions allowed has reached.";
                    default:
                        return "Error.";
                }
        }
    }
}
