package com.minarath.tv.confielder_lge.confielder_hardware.confielder_IRBlaster;

import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.os.Build;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.Html;
import android.util.Log;

import com.minarath.tv.confielder_uei.confielder_control.confielder_Device;
import com.minarath.tv.confielder_uei.confielder_control.confielder_IControl;
import com.minarath.tv.confielder_uei.confielder_control.confielder_IControlCallback;
import com.minarath.tv.confielder_uei.confielder_control.confielder_ILearnIRStatusCallback;
import com.minarath.tv.confielder_uei.confielder_control.confielder_IRAction;
import com.minarath.tv.confielder_uei.confielder_control.confielder_ISetup;
import com.minarath.tv.confielder_uei.confielder_control.confielder_ISetupReadyCallback;

import java.io.InputStream;
import java.util.ArrayList;

import com.minarath.tv.confielder_lge.confielder_qremote.confielder_settings.confielder_provider.confielder_QRemoteSettingsContract;


public class confielder_IRBlaster {
    RemoteException e;
    protected static final String INTENT_ACTION_DEVICE_ADDED = "com.lge.qremote.action.DeviceAdded";
    private static final int INVALID_SESSION = 6;
    @Deprecated
    public static final String IRBLASTER = "android.hardware.irblaster";
    @Deprecated
    public static final String IRBLASTER_PERM = "android.hardware.IRBLASTERpermission";
    private static final int MAXRETRIES = 3;
    private static final int QUICKSETSERVICESNOTACTIVATED = -1;
    protected static final String RES_DEV_ID = "openSdkDeviceId";
    private static final String TAG = "confielder_IRBlaster";
    private static final String UEICONTROLPACKAGE = "com.uei.lg.quicksetsdk";
    private static String UEICONTROLPACKAGE_CURRENT = null;
    private static final String UEICONTROLPACKAGE_LITE = "com.uei.lg.quicksetsdk.lite";
    private static final String UEICONTROLPACKAGE_MAXQ = "com.uei.lg.quicksetsdk.maxq616";
    private static final String UEICONTROLSERVICENAME = "com.uei.control.Service";
    protected static boolean sLog = false;
    private static final String sQremotePckgName = "com.lge.qremote";
    private final int DEFAULT_IR_DURATION = 300;
    private final int MAX_DEVICES_TO_STORE = 24;
    protected Context mContext;
    ServiceConnection mControlServiceConnection = new ServiceConnection() {
        public void onServiceDisconnected(ComponentName componentName) {
            confielder_IRBlaster.this.handleServicesDisconnected();
            if (confielder_IRBlaster.sLog) {
                Log.d(confielder_IRBlaster.TAG, "confielder_IControl disconnected");
            }
            confielder_IRBlaster.this.mIControl = null;
            confielder_IRBlaster.this.mIControlConnected = false;
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (confielder_IRBlaster.sLog) {
                Log.d(confielder_IRBlaster.TAG, "confielder_IControl Connected");
            }
            confielder_IRBlaster.this.mIControl = new confielder_IControl(iBinder);
            confielder_IRBlaster.this.mIControlConnected = true;
            try {
                confielder_IRBlaster.this.mIControl.registerCallback(confielder_IRBlaster.this.mIControlCallback);
            } catch (RemoteException e) {
                Log.e(confielder_IRBlaster.TAG, e.getMessage());
                e.printStackTrace();
            }
        }
    };
    protected BroadcastReceiver mDeviceAddedReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                int intExtra = intent.getIntExtra(confielder_IRBlaster.RES_DEV_ID, -1);
                if (intExtra != -1) {
                    String str = confielder_IRBlaster.TAG;
                    StringBuilder sb = new StringBuilder("Broadcast received, added device ID broad = ");
                    sb.append(intExtra);
                    Log.i(str, sb.toString());
                    if (confielder_IRBlaster.this.mIRBlasterCallback != null) {
                        confielder_IRBlaster.this.mIRBlasterCallback.newDeviceId(intExtra);
                    } else {
                        Log.e(confielder_IRBlaster.TAG, "confielder_IRBlasterCallback is not registered.");
                    }
                } else {
                    Log.e(confielder_IRBlaster.TAG, "Broadcast received, wrong device ID.");
                }
            }
        }
    };
    private confielder_Device[] mDevices = null;
    private boolean mHasValidSession;
    protected confielder_IControl mIControl;
    confielder_IControlCallback mIControlCallback = new confielder_IControlCallback() {
        public IBinder asBinder() {
            return null;
        }

        public void devicesChanged() throws RemoteException {
            if (confielder_IRBlaster.sLog) {
                Log.d(confielder_IRBlaster.TAG, " device LIst changed  IControCallback");
            }
        }
    };
    protected boolean mIControlConnected = false;
    /* access modifiers changed from: private */
    public boolean mIControlInit = false;
    confielder_ILearnIRStatusCallback mILearnIRStatusCallback = new confielder_ILearnIRStatusCallback.Stub() {
        public void learnIRReady(int i) throws RemoteException {
        }

        public void learnIRCompleted(int i) throws RemoteException {
            if (confielder_IRBlaster.sLog) {
                Log.d(confielder_IRBlaster.TAG, "learnIRCompleted  ReadyCallback...");
            }
            confielder_IRBlaster.this.mIRBlasterCallback.learnIRCompleted(i);
        }
    };
    confielder_IRBlasterCallback mIRBlasterCallback;
    public confielder_ISetupReadyCallback mIServicesReadyCallback = new confielder_ISetupReadyCallback.Stub() {
        public void QSSetupIsReady(int i) {
            try {
                if (confielder_IRBlaster.sLog) {
                    String str = confielder_IRBlaster.TAG;
                    StringBuilder sb = new StringBuilder("QS SDK Services callback: StatusCode = ");
                    sb.append(i);
                    Log.d(str, sb.toString());
                }
                boolean access$5 = confielder_IRBlaster.this.activateQuicksetService();
                confielder_IRBlaster.this.mLastResult = confielder_IRBlaster.this.getLastResultCodeSetup();
                if (confielder_IRBlaster.sLog) {
                    String str2 = confielder_IRBlaster.TAG;
                    StringBuilder sb2 = new StringBuilder("Activate quicksetservices ");
                    sb2.append(access$5);
                    sb2.append(" : ");
                    sb2.append(confielder_IRBlaster.this.mLastResult);
                    Log.d(str2, sb2.toString());
                }
                confielder_IRBlaster.this.mQSServicesReady = true;
                if (confielder_IRBlaster.sLog) {
                    Log.d(confielder_IRBlaster.TAG, "Unregistering confielder_ISetupReadyCallback...");
                }
                confielder_IRBlaster.this.mISetup.unregisterSetupReadyCallback(confielder_IRBlaster.this.mIServicesReadyCallback);
                if (confielder_IRBlaster.sLog) {
                    Log.d(confielder_IRBlaster.TAG, "HW ready callback unregistered.");
                }
                if (confielder_IRBlaster.this.mISetupConnected && confielder_IRBlaster.this.mIControlConnected && confielder_IRBlaster.this.mQSServicesReady) {
                    confielder_IRBlaster.this.mIRBlasterCallback.IRBlasterReady();
                    if (confielder_IRBlaster.sLog) {
                        Log.i(confielder_IRBlaster.TAG, "Blaster ready callback dispatched.");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    protected confielder_ISetup mISetup;
    public boolean mISetupConnected = false;
    protected int mLastResult = 1;
    protected confielder_MoreFuncsInitializer mMoreFuncsInitializer = null;
    public boolean mQSServicesReady = false;
    private int mRetries = 0;
    protected long mSessionId = 0;
    private ServiceConnection mSetupServiceConnection = new ServiceConnection() {
        public void onServiceDisconnected(ComponentName componentName) {
            if (confielder_IRBlaster.sLog) {
                Log.d(confielder_IRBlaster.TAG, "confielder_ISetup disconnected.");
            }
            confielder_IRBlaster.this.mISetupConnected = false;
            confielder_IRBlaster.this.mQSServicesReady = false;
            confielder_IRBlaster.this.mISetup = null;
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (confielder_IRBlaster.sLog) {
                Log.d(confielder_IRBlaster.TAG, "Connect setup service...");
            }
            try {
                confielder_IRBlaster.this.mISetup = new confielder_ISetup(iBinder);
                confielder_IRBlaster.this.mISetupConnected = true;
                confielder_IRBlaster.this.mSessionId = confielder_IRBlaster.this.getSession();
                confielder_IRBlaster.this.mLastResult = confielder_IRBlaster.this.getLastResultCodeSetup();
                if (confielder_IRBlaster.sLog) {
                    String str = confielder_IRBlaster.TAG;
                    StringBuilder sb = new StringBuilder("Setup service session ID obtained [");
                    sb.append(confielder_IRBlaster.this.mSessionId);
                    sb.append("].");
                    Log.i(str, sb.toString());
                }
                confielder_IRBlaster.this.mISetup.registerSetupReadyCallback(confielder_IRBlaster.this.mIServicesReadyCallback);
                if (confielder_IRBlaster.sLog) {
                    Log.d(confielder_IRBlaster.TAG, "HW ready callback registered.");
                }
            } catch (Exception e) {
                if (confielder_IRBlaster.sLog) {
                    Log.d(confielder_IRBlaster.TAG, e.toString());
                }
            }
            confielder_IRBlaster.this.initServices();
        }
    };

    protected static String getUeiControlServiceName() {
        return UEICONTROLSERVICENAME;
    }

    @Deprecated
    public static String getUeicontrolservicename() {
        return UEICONTROLSERVICENAME;
    }

    public int getDefaultDuration() {
        return 300;
    }

    @Deprecated
    public confielder_IRBlaster() {
        Log.d(TAG, "confielder_IRBlaster()");
    }

    protected confielder_IRBlaster(Context context, confielder_IRBlasterCallback iRBlasterCallback) {
        if (sLog) {
            Log.d(TAG, "confielder_IRBlaster(context, callback)");
        }
        this.mContext = context;
        this.mMoreFuncsInitializer = new confielder_MoreFuncsInitializer();
        prepareInternals(iRBlasterCallback);
    }

    protected confielder_IRBlaster(Context context, confielder_IRBlasterCallback iRBlasterCallback, InputStream inputStream) {
        if (sLog) {
            Log.d(TAG, "confielder_IRBlaster(context, callback)");
        }
        this.mContext = context;
        this.mMoreFuncsInitializer = new confielder_MoreFuncsInitializer(inputStream);
        prepareInternals(iRBlasterCallback);
    }

    public void prepareInternals(confielder_IRBlasterCallback iRBlasterCallback) {
        registerIRBlasterReadyCallback(iRBlasterCallback);
        bindServices(this.mContext);
        isInitialized();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(INTENT_ACTION_DEVICE_ADDED);
        this.mContext.registerReceiver(this.mDeviceAddedReceiver, intentFilter);
    }

    public static confielder_IRBlaster getIRBlaster(Context context, confielder_IRBlasterCallback iRBlasterCallback) {
        if (isSdkSupported(context)) {
            return new confielder_IRBlaster(context, iRBlasterCallback);
        }
        return null;
    }

    public confielder_Device[] getDevices() throws RemoteException {
        Context context = this.mContext;
        if (hasValidControl()) {
            Cursor query = context.getContentResolver().query(confielder_QRemoteSettingsContract.DevicesColumns.CONTENT_URI_NO_ROOM, new String[]{"device_id", confielder_QRemoteSettingsContract.DevicesColumns.ROOM_ID}, "device_type_name <> 'Flexible'", null, null);
            if (query != null) {
                try {
                    if (query.getCount() > 0) {
                        ArrayList arrayList = new ArrayList(query.getCount());
                        while (query.moveToNext()) {
                            arrayList.add(Integer.valueOf(query.getInt(query.getColumnIndex("device_id"))));
                        }
                        if (sLog) {
                            Log.d(TAG, "getDevices of control.");
                        }
                        confielder_Device[] devices = this.mIControl.getDevices();
                        this.mLastResult = getLastResultCodeControl();
                        if (devices == null || devices.length <= 0) {
                            if (query != null) {
                                query.close();
                            }
                            return null;
                        }
                        confielder_Device[] convertDeviceArrayFromInternal = confielder_TypeConverter.convertDeviceArrayFromInternal(devices);
                        ArrayList arrayList2 = new ArrayList();
                        for (confielder_Device device : convertDeviceArrayFromInternal) {
                            if (device != null && arrayList.contains(Integer.valueOf(device.Id))) {
                                arrayList2.add(device);
                            }
                        }
                        if (sLog) {
                            String str = TAG;
                            StringBuilder sb = new StringBuilder("Obtained devices count [");
                            sb.append(arrayList2.size());
                            sb.append("].");
                            Log.d(str, sb.toString());
                        }
                        this.mDevices = new confielder_Device[arrayList2.size()];
                        this.mDevices = (confielder_Device[]) arrayList2.toArray(this.mDevices);
                        confielder_Device[] deviceArr = this.mDevices;
                        if (query != null) {
                            query.close();
                        }
                        return deviceArr;
                    }
                } finally {
                    if (query != null) {
                        query.close();
                    }
                }
            }
            if (sLog) {
                Log.d(TAG, "There are no items in the tblDevices.");
            }
            if (query != null) {
                query.close();
            }
            return null;
        }
        if (sLog) {
            Log.w(TAG, "getDevices failed, invalid control environment settings.");
        }
        return null;
    }


    public int sendIRPattern(int i, int[] iArr) {
        try {
            this.mLastResult = confielder_ResultCodeConverter.matchToExternalResultCode(this.mIControl.sendIRPattern(i, iArr));
            String str = TAG;
            StringBuilder sb = new StringBuilder("Send IR Pattern: ");
            sb.append(this.mLastResult);
            sb.append(" - ");
            sb.append(confielder_ResultCode.getString(this.mLastResult));
            Log.d(str, sb.toString());
        } catch (RemoteException e) {
            this.mLastResult = 1;
            e.printStackTrace();
        }
        return this.mLastResult;
    }

    public int sendIR(confielder_IRAction iRAction) {
        this.mLastResult = 1;
        try {
            if (hasValidControl()) {
                this.mLastResult = this.mIControl.sendIR(confielder_TypeConverter.convertIrActionToInternal(iRAction));
                this.mLastResult = confielder_ResultCodeConverter.matchToExternalResultCode(this.mLastResult);
            }
            if (sLog) {
                String str = TAG;
                StringBuilder sb = new StringBuilder("IR sent, result: ");
                sb.append(getResultCodeString(this.mLastResult));
                Log.d(str, sb.toString());
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            String str2 = TAG;
            StringBuilder sb2 = new StringBuilder("Send IR failed: ");
            sb2.append(e.getMessage());
            Log.e(str2, sb2.toString());
        }
        return this.mLastResult;
    }


    public int stopIR() {
        this.mLastResult = 1;
        try {
            if (hasValidControl()) {
                this.mLastResult = this.mIControl.stopIR();
                this.mLastResult = confielder_ResultCodeConverter.matchToExternalResultCode(this.mLastResult);
            }
            if (sLog) {
                String str = TAG;
                StringBuilder sb = new StringBuilder("IR stopped, result: ");
                sb.append(getResultCodeString(this.mLastResult));
                Log.d(str, sb.toString());
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            String str2 = TAG;
            StringBuilder sb2 = new StringBuilder("Stop IR failed: ");
            sb2.append(e.getMessage());
            Log.e(str2, sb2.toString());
        }
        return this.mLastResult;
    }


    public void registerLearnIrStatusCallback() throws RemoteException {
        if (hasValidSession()) {
            this.mISetup.registerLearnIRStatusCallback(this.mILearnIRStatusCallback);
        }
    }

    public String getResultCodeString(int i) {
        return confielder_ResultCode.getString(i);
    }


    public int getLastResultCodeControl() {
        int i = 1;
        try {
            if (!hasValidControl()) {
                return 1;
            }
            int lastResultcode = this.mIControl.getLastResultcode();
            return confielder_ResultCodeConverter.matchToExternalResultCode(lastResultcode);
        } catch (RemoteException e2) {
            e = e2;
            e.printStackTrace();
            String str2 = TAG;
            StringBuilder sb2 = new StringBuilder("getLastResultcode() failed: ");
            sb2.append(e.getMessage());
            Log.e(str2, sb2.toString());
            return i;
        }
    }

    public int getLastResultCodeSetup() {
        try {
            if (hasValidSession()) {
                return this.mISetup.getLastResultcode();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 1;
    }

    public static boolean isSdkSupported(Context context) {
        boolean z;
        boolean z2;
        boolean z3;
        if (!isInstalledApp(context, sQremotePckgName)) {
            Intent intent = new Intent("com.lge.appbox.commonservice.update");
            intent.putExtra("packagename", sQremotePckgName);
            intent.putExtra("type", "download");
            context.startService(intent);
            return false;
        } else if (isDisabledApp(context, sQremotePckgName)) {
            Intent intent2 = new Intent("com.lge.appbox.commonservice.update");
            intent2.putExtra("packagename", sQremotePckgName);
            intent2.putExtra("type", "enable");
            context.startService(intent2);
            return false;
        } else {
            try {
                context.getPackageManager().getPackageInfo(UEICONTROLPACKAGE, PackageManager.GET_ACTIVITIES);
                UEICONTROLPACKAGE_CURRENT = UEICONTROLPACKAGE;
                z = true;
            } catch (NameNotFoundException e) {
                e.printStackTrace();
                if (sLog) {
                    Log.d(TAG, "services UEICONTROLPACKAGE not available");
                }
                z = false;
            }
            try {
                context.getPackageManager().getPackageInfo(UEICONTROLPACKAGE_MAXQ, PackageManager.GET_ACTIVITIES);
                UEICONTROLPACKAGE_CURRENT = UEICONTROLPACKAGE_MAXQ;
                z2 = true;
            } catch (NameNotFoundException e2) {
                e2.printStackTrace();
                if (sLog) {
                    Log.d(TAG, "services UEICONTROLPACKAGE_MAXQ not available");
                }
                z2 = false;
            }
            try {
                context.getPackageManager().getPackageInfo(UEICONTROLPACKAGE_LITE, PackageManager.GET_ACTIVITIES);
                UEICONTROLPACKAGE_CURRENT = UEICONTROLPACKAGE_LITE;
                z3 = true;
            } catch (NameNotFoundException e3) {
                e3.printStackTrace();
                if (sLog) {
                    Log.d(TAG, "services UEICONTROLPACKAGE_LITE not available");
                }
                z3 = false;
            }
            if (!z && !z2 && !z3) {
                String str = Build.MODEL;
                Builder builder = new Builder(context);
                builder.setTitle(str);
                builder.setMessage(Html.fromHtml("<b>QuickSetSDK is not installed</b>")).setCancelable(false).setPositiveButton(Html.fromHtml("<b>OK</b>"), new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                builder.create().show();
            }
            if (z || z2 || z3) {
                return true;
            }
            return false;
        }
    }

    public void registerIRBlasterReadyCallback(confielder_IRBlasterCallback iRBlasterCallback) {
        this.mIRBlasterCallback = iRBlasterCallback;
    }

    public long getSession() throws RemoteException {
        if (hasValidSession()) {
            return this.mISetup.getSession();
        }
        return 0;
    }

    @Deprecated
    public Context getmContext() {
        return this.mContext;
    }

    @Deprecated
    public void setmContext(Context context) {
        this.mContext = context;
    }

    @Deprecated
    public static String getUeicontrolpackage() {
        return UEICONTROLPACKAGE_CURRENT;
    }

    protected static String getUeiControlPackage() {
        return UEICONTROLPACKAGE_CURRENT;
    }

    /* access modifiers changed from: protected */
    public void bindServices(Context context) {
        this.mIControl = null;
        Intent intent = new Intent(confielder_IControl.DESCRIPTOR);
        intent.setClassName(getUeiControlPackage(), getUeiControlServiceName());
        context.bindService(intent, this.mControlServiceConnection, Context.BIND_AUTO_CREATE);
        this.mISetup = null;
        Intent intent2 = new Intent(confielder_ISetup.DESCRIPTOR);
        intent2.setClassName(getUeiControlPackage(), getUeiControlServiceName());
        context.bindService(intent2, this.mSetupServiceConnection, Context.BIND_AUTO_CREATE);
        if (sLog) {
            Log.d(TAG, "confielder_IRBlaster bindServices() ");
        }
    }

    public void handleServicesDisconnected() {
        bindServices(this.mContext);
        isInitialized();
    }

    private void isInitialized() {
        new Thread() {
            public void run() {
                boolean z;
                long currentTimeMillis = System.currentTimeMillis();
                try {
                    confielder_IRBlaster.this.mContext.getPackageManager().getPackageInfo(confielder_IRBlaster.getUeiControlPackage(), PackageManager.GET_ACTIVITIES);
                    z = true;
                } catch (NameNotFoundException unused) {
                    z = false;
                }
                if (z) {
                    while (System.currentTimeMillis() - currentTimeMillis < 10000) {
                        try {
                            if (confielder_IRBlaster.sLog) {
                                String str = confielder_IRBlaster.TAG;
                                StringBuilder sb = new StringBuilder("Setup service ready [");
                                sb.append(confielder_IRBlaster.this.mQSServicesReady);
                                sb.append("] and ");
                                sb.append("connected [");
                                sb.append(confielder_IRBlaster.this.mISetupConnected);
                                sb.append("].");
                                Log.d(str, sb.toString());
                                String str2 = confielder_IRBlaster.TAG;
                                StringBuilder sb2 = new StringBuilder("Control service connected [");
                                sb2.append(confielder_IRBlaster.this.mIControlConnected);
                                sb2.append("] and");
                                sb2.append(" initialized [");
                                sb2.append(confielder_IRBlaster.this.mIControlInit);
                                sb2.append("].");
                                Log.d(str2, sb2.toString());
                            }
                            if (confielder_IRBlaster.this.stopIR() == 0) {
                                confielder_IRBlaster.this.mIControlInit = true;
                            }
                            if (!confielder_IRBlaster.this.mISetupConnected || !confielder_IRBlaster.this.mIControlConnected || !confielder_IRBlaster.this.mQSServicesReady || !confielder_IRBlaster.this.mIControlInit) {
                                Thread.sleep(50);
                            } else {
                                confielder_IRBlaster.this.mIRBlasterCallback.IRBlasterReady();
                                return;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            return;
                        }
                    }
                } else if (confielder_IRBlaster.sLog) {
                    Log.e(confielder_IRBlaster.TAG, "No IR blaster SDK found.");
                }
            }
        }.start();
    }

    public boolean activateQuicksetService() {
        try {
            if (hasValidSession()) {
                return this.mISetup.activateQuicksetService(this.mMoreFuncsInitializer.getMoreFuncsToken());
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean hasValidSession() throws RemoteException {
        this.mHasValidSession = false;
        if (sLog) {
            Log.d(TAG, "has ValidSession() start");
        }
        if (this.mMoreFuncsInitializer == null) {
            this.mMoreFuncsInitializer = new confielder_MoreFuncsInitializer();
        }
        if (this.mMoreFuncsInitializer == null || this.mMoreFuncsInitializer.getMoreFuncsToken() == null) {
            if (sLog) {
                Log.w(TAG, "Additional functionalities are not initialized to support setup operations.");
            }
            return this.mHasValidSession;
        } else if (this.mISetup != null) {
            int validateSession = this.mISetup.validateSession(this.mSessionId);
            if (validateSession != 0) {
                if (sLog) {
                    String str = TAG;
                    StringBuilder sb = new StringBuilder("Invalid session result: ");
                    sb.append(validateSession);
                    Log.e(str, sb.toString());
                }
                if (validateSession == 6) {
                    if (sLog) {
                        Log.i(TAG, "Renew setup session.");
                    }
                    long session = this.mISetup.getSession();
                    this.mSessionId = session;
                    if (session != 0) {
                        this.mHasValidSession = true;
                    } else {
                        throw new RemoteException("confielder_IRBlaster not ready");
                    }
                } else if (validateSession == -1) {
                    if (activateQuicksetService()) {
                        if (sLog) {
                            Log.d(TAG, "Blaster activated.");
                        }
                        return true;
                    }
                    if (sLog) {
                        Log.e(TAG, "Failed to activate blaster. ");
                    }
                    throw new RemoteException("confielder_IRBlaster not ready");
                } else if (validateSession != 9) {
                    throw new RemoteException("confielder_IRBlaster not ready");
                } else if (this.mRetries < 3) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    this.mRetries++;
                    return hasValidSession();
                } else {
                    this.mRetries = 0;
                    throw new RemoteException("confielder_IRBlaster not ready");
                }
            } else {
                this.mHasValidSession = true;
            }
            return this.mHasValidSession;
        } else {
            handleServicesDisconnected();
            return false;
        }
    }

    private static boolean isInstalledApp(Context context, String str) {
        try {
            context.getPackageManager().getPackageInfo(str, 0);
            return true;
        } catch (NameNotFoundException unused) {
            return false;
        }
    }

    private static boolean isDisabledApp(Context context, String str) {
        try {
            if (!context.getPackageManager().getApplicationInfo(str, 0).enabled) {
                return true;
            }
            return false;
        } catch (NameNotFoundException unused) {
            return false;
        }
    }

    private boolean hasValidControl() {
        return this.mIControl != null && this.mIControlConnected;
    }

    public void initServices() {
        if (this.mISetup != null) {
            try {
                if (this.mISetup.activateQuicksetService(this.mMoreFuncsInitializer.getMoreFuncsToken())) {
                    Log.d(TAG, "Activated Quickset.");
                    registerLearnIrStatusCallback();
                } else {
                    getLastResultCode("Failed to activated Quickset ");
                }
                this.mLastResult = getLastResultCodeSetup();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "confielder_ISetup is initialized.");
            if (this.mISetupConnected && this.mIControlConnected && this.mQSServicesReady) {
                this.mIRBlasterCallback.IRBlasterReady();
            }
        }
    }

    private void getLastResultCode(String str) {
        if (this.mISetup != null) {
            int i = 1;
            try {
                i = this.mISetup.getLastResultcode();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            String str2 = TAG;
            StringBuilder sb = new StringBuilder(String.valueOf(str));
            sb.append(" Last result code = ");
            sb.append(i);
            sb.append(" - ");
            sb.append(confielder_ResultCode.getString(i));
            Log.d(str2, sb.toString());
        }
    }
}
