package com.minarath.tv.confielder_lge.confielder_hardware.confielder_IRBlaster;

public interface confielder_IRBlasterCallback {
    void IRBlasterReady();

    void learnIRCompleted(int i);

    void newDeviceId(int i);
}
