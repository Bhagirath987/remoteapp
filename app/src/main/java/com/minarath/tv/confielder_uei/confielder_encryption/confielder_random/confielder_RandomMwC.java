package com.minarath.tv.confielder_uei.confielder_encryption.confielder_random;

public class confielder_RandomMwC {
    private final long a = 4294957665L;
    private long x = (System.nanoTime() & 4294967295L);

    public confielder_RandomMwC(long j) {
        this.x = j;
    }

    public int nextInt() {
        this.x = ((this.x & 4294967295L) * 4294957665L) + (this.x >>> 32);
        return (int) this.x;
    }
}
