package com.minarath.tv.confielder_uei.confielder_control;

import android.os.Parcel;
import android.os.Parcelable;

public class confielder_LearnedIRData implements Parcelable {
    public static final Creator<confielder_LearnedIRData> CREATOR = new Creator<confielder_LearnedIRData>() {
        public confielder_LearnedIRData createFromParcel(Parcel parcel) {
            return new confielder_LearnedIRData(parcel, (confielder_LearnedIRData) null);
        }

        public confielder_LearnedIRData[] newArray(int i) {
            return new confielder_LearnedIRData[i];
        }
    };
    public byte[] Data;
    public int Id;

    public int describeContents() {
        return 0;
    }

    public confielder_LearnedIRData(int i, byte[] bArr) {
        this.Id = 0;
        this.Data = null;
        this.Id = i;
        this.Data = bArr;
        if (this.Data == null) {
            this.Data = new byte[0];
        }
    }

    private confielder_LearnedIRData(Parcel parcel) {
        this.Id = 0;
        this.Data = null;
        readFromParcel(parcel);
    }

    confielder_LearnedIRData(Parcel parcel, confielder_LearnedIRData learnedIRData) {
        this(parcel);
    }

    public void writeToParcel(Parcel parcel, int i) {
        try {
            parcel.writeInt(this.Id);
            parcel.writeByteArray(this.Data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void readFromParcel(Parcel parcel) {
        try {
            this.Id = parcel.readInt();
            this.Data = parcel.createByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
