package com.minarath.tv.confielder_uei.confielder_control;

import android.os.Parcel;
import android.os.Parcelable;

public class confielder_IRAction implements Parcelable {
    public static final Creator<confielder_IRAction> CREATOR = new Creator<confielder_IRAction>() {
        public confielder_IRAction createFromParcel(Parcel parcel) {
            return new confielder_IRAction(parcel, null);
        }

        public confielder_IRAction[] newArray(int i) {
            return new confielder_IRAction[i];
        }
    };
    public int DeviceId;
    public int Duration;
    public int Function;

    public int describeContents() {
        return 0;
    }

    public confielder_IRAction(int i, int i2, int i3) {
        this.DeviceId = 0;
        this.Function = 0;
        this.Duration = 0;
        this.DeviceId = i;
        this.Function = i2;
        this.Duration = i3;
    }

    private confielder_IRAction(Parcel parcel) {
        this.DeviceId = 0;
        this.Function = 0;
        this.Duration = 0;
        readFromParcel(parcel);
    }

    confielder_IRAction(Parcel parcel, confielder_IRAction iRAction) {
        this(parcel);
    }

    public void writeToParcel(Parcel parcel, int i) {
        try {
            parcel.writeInt(this.DeviceId);
            parcel.writeInt(this.Function);
            parcel.writeInt(this.Duration);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void readFromParcel(Parcel parcel) {
        try {
            this.DeviceId = parcel.readInt();
            this.Function = parcel.readInt();
            this.Duration = parcel.readInt();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
