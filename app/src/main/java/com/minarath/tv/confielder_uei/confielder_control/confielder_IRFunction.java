package com.minarath.tv.confielder_uei.confielder_control;

import android.os.Parcel;
import android.os.Parcelable;

public class confielder_IRFunction implements Parcelable {
    public static final Creator<confielder_IRFunction> CREATOR = new Creator<confielder_IRFunction>() {
        public confielder_IRFunction createFromParcel(Parcel parcel) {
            return new confielder_IRFunction(parcel, null);
        }

        public confielder_IRFunction[] newArray(int i) {
            return new confielder_IRFunction[i];
        }
    };
    public int Id;
    public boolean IsLearned;
    public Short LearnedCode;
    public String Name;

    public int describeContents() {
        return 0;
    }

    public confielder_IRFunction() {
        this.Name = "";
        this.Id = 0;
        this.IsLearned = false;
        this.LearnedCode = Short.valueOf((short) 0);
    }

    public confielder_IRFunction(String name, int id, boolean isLearned, Short learnedCode) {
        Id = id;
        IsLearned = isLearned;
        LearnedCode = learnedCode;
        Name = name;
    }

    private confielder_IRFunction(Parcel parcel) {
        this.Name = "";
        this.Id = 0;
        this.IsLearned = false;
        this.LearnedCode = Short.valueOf((short) 0);
        readFromParcel(parcel);
    }

     confielder_IRFunction(Parcel parcel, confielder_IRFunction iRFunction) {
        this(parcel);
    }

    public void writeToParcel(Parcel parcel, int i) {
        try {
            parcel.writeInt(this.Id);
            parcel.writeString(this.Name);
            parcel.writeInt(this.IsLearned ? 1 : 0);
            parcel.writeInt(this.LearnedCode.shortValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void readFromParcel(Parcel parcel) {
        try {
            this.Id = parcel.readInt();
            this.Name = parcel.readString();
            boolean z = true;
            if (parcel.readInt() != 1) {
                z = false;
            }
            this.IsLearned = z;
            this.LearnedCode = Short.valueOf((short) parcel.readInt());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String toString() {
        return this.Name;
    }
}
