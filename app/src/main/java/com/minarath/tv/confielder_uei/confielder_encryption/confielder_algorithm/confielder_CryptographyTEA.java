package com.minarath.tv.confielder_uei.confielder_encryption.confielder_algorithm;

public class confielder_CryptographyTEA {
    private static final int DECRYPTSUM = -957401312;
    private static final int DELTA = -1640531527;
    private int[] _privateSeed = new int[4];

    public byte[] encryptData(byte[] bArr, byte[] bArr2) throws RuntimeException {
        if(bArr2 == null) {
            throw new RuntimeException("Invalid key: Key was null");
        }
        else if(bArr2.length < 16) {
            throw new RuntimeException("Invalid key: Length was less than 16 bytes");
        }
        else if(bArr != null) {
            makeSeed(bArr2);
            int[] iArr = new int[((((bArr.length / 8) + (bArr.length % 8 == 0 ? 0 : 1)) * 2) + 1)];
            iArr[0] = bArr.length;
            packInputData(bArr, iArr, 1);
            scrambleData(iArr);
            return unpackInputData(iArr, 0, iArr.length * 4);
        }
        else {
            throw new RuntimeException("No data to encrypt!");
        }
    }


    private void makeSeed(byte[] bArr) {
        if(bArr != null) {
            this._privateSeed = new int[4];
            int i = 0;
            int i2 = 0;
            while(i < 4) {
                int i3 = i2 + 1;
                int i4 = i3 + 1;
                byte b = (byte) ((bArr[i2] & 255) | ((bArr[i3] & 255) << 8));
                int i5 = i4 + 1;
                byte b2 = (byte) (b | ((bArr[i4] & 255) << 16));
                int i6 = i5 + 1;
                this._privateSeed[i] = b2 | ((bArr[i5] & 255) << 24);
                i++;
                i2 = i6;
            }
        }
    }

    public void scrambleData(int[] iArr) {
        for(int i = 1; i < iArr.length; i += 2) {
            int i2 = 32;
            int i3 = iArr[i];
            int i4 = i + 1;
            int i5 = iArr[i4];
            int i6 = 0;
            while(true) {
                int i7 = i2 - 1;
                if(i2 <= 0) {
                    break;
                }
                i6 += DELTA;
                i3 += (((i5 << 4) + this._privateSeed[0]) ^ i5) + ((i5 >>> 5) ^ i6) + this
                        ._privateSeed[1];
                i5 += (((i3 << 4) + this._privateSeed[2]) ^ i3) + ((i3 >>> 5) ^ i6) + this
                        ._privateSeed[3];
                i2 = i7;
            }
            iArr[i] = i3;
            iArr[i4] = i5;
        }
    }




    public void packInputData(byte[] bArr, int[] iArr, int i) {
        int r3;
        iArr[i] = 0;
        int i2 = i;
        int i3 = 0;
        int r32 = 24;
        while(i3 < bArr.length) {
            iArr[i2] = iArr[i2] | ((bArr[i3] & 255) << r32);
            if(r32 == 0) {
                i2++;
                if(i2 < iArr.length) {
                    iArr[i2] = 0;
                }
                r3 = 24;
            }
            else {
                r3 = r32 - 8;
            }
            i3++;
            r32 = r3;
        }
    }

    public byte[] unpackInputData(int[] iArr, int i, int i2) {
        byte[] bArr = new byte[i2];
        int i3 = i;
        int i4 = 0;
        for(int i5 = 0; i5 < i2; i5++) {
            bArr[i5] = (byte) ((iArr[i3] >> (24 - (i4 * 8))) & 255);
            i4++;
            if(i4 == 4) {
                i3++;
                i4 = 0;
            }
        }
        return bArr;
    }
}
