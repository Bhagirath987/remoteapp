package com.minarath.tv.confielder_uei.confielder_control;

import android.os.Parcel;
import android.os.Parcelable;

public class confielder_SetupMapResult implements Parcelable {
    public static final Creator<confielder_SetupMapResult> CREATOR = new Creator<confielder_SetupMapResult>() {
        public confielder_SetupMapResult createFromParcel(Parcel parcel) {
            return new confielder_SetupMapResult(parcel);
        }

        public confielder_SetupMapResult[] newArray(int i) {
            return new confielder_SetupMapResult[i];
        }
    };
    public int CurrentBestMatchCodeIndex = -1;
    public int CurrentCandidatesCount = -1;
    public int CurrentTestKeyIndex = -1;
    public int FunctionId = -1;
    public String FunctionLabel = "";
    public int Status = -1;

    public int describeContents() {
        return 0;
    }




    public confielder_SetupMapResult(Parcel parcel) {
        readFromParcel(parcel);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.Status);
        parcel.writeInt(this.FunctionId);
        parcel.writeInt(this.CurrentBestMatchCodeIndex);
        parcel.writeInt(this.CurrentTestKeyIndex);
        parcel.writeInt(this.CurrentCandidatesCount);
        parcel.writeString(this.FunctionLabel);
    }

    public void readFromParcel(Parcel parcel) {
        try {
            this.Status = parcel.readInt();
            this.FunctionId = parcel.readInt();
            this.CurrentBestMatchCodeIndex = parcel.readInt();
            this.CurrentTestKeyIndex = parcel.readInt();
            this.CurrentCandidatesCount = parcel.readInt();
            this.FunctionLabel = parcel.readString();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
