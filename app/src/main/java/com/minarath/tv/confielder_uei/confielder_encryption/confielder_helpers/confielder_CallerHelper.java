package com.minarath.tv.confielder_uei.confielder_encryption.confielder_helpers;

import android.util.Base64;
import com.minarath.tv.confielder_uei.confielder_encryption.confielder_algorithm.confielder_CryptographyTEA;

public class confielder_CallerHelper {
    private int errorMs = 1000;
    private int randomArraySize = 32;
    private int windowMs = 5000;


    public String getEncryptedStringBase64(byte[] bArr) {
        return Base64.encodeToString(getEncryptedKey(bArr), 0);
    }

    public byte[] getEncryptedKey(byte[] bArr) {
        confielder_RandomHelper randomHelper = new confielder_RandomHelper();
        randomHelper.setTimeMs(System.currentTimeMillis());
        randomHelper.setWindowMs(this.windowMs);
        randomHelper.setErrorMs(this.errorMs);
        randomHelper.setRandomArraySize(this.randomArraySize);
        randomHelper.calculateRandoms();
        byte[] bArr2 = null;
        try {
            return new confielder_CryptographyTEA().encryptData(randomHelper.getRandomBytes(), bArr);
        } catch (Exception e) {
            e.printStackTrace();
            return new byte[0];
        }
    }
}
