package com.minarath.tv.confielder_uei.confielder_control;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface confielder_IControlCallback extends IInterface {

    public static abstract class Stub extends Binder implements confielder_IControlCallback {
        private static final String DESCRIPTOR = "com.uei.control.confielder_IControlCallback";
        static final int TRANSACTION_devicesChanged = 1;

        private static class Proxy implements confielder_IControlCallback {
            private IBinder mRemote;


            Proxy(IBinder iBinder) {
                this.mRemote = iBinder;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void devicesChanged() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(1, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }
        }

        public IBinder asBinder() {
            return this;
        }


        public static confielder_IControlCallback asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof confielder_IControlCallback)) {
                return new Proxy(iBinder);
            }
            return (confielder_IControlCallback) queryLocalInterface;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                parcel.enforceInterface(DESCRIPTOR);
                devicesChanged();
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString(DESCRIPTOR);
                return true;
            }
        }
    }

    void devicesChanged() throws RemoteException;
}
