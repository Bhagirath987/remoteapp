package com.minarath.tv.confielder_uei.confielder_control;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;

public class confielder_IControl {
    public static final String DESCRIPTOR = "com.uei.control.confielder_IControl";
    private IBinder _controlService = null;

    public confielder_IControl(IBinder iBinder) {
        this._controlService = iBinder;
    }

    public int sendIR(confielder_IRAction iRAction) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        if (this._controlService != null) {
            try {
                obtain.writeInterfaceToken(DESCRIPTOR);
                if (iRAction != null) {
                    obtain.writeInt(1);
                    iRAction.writeToParcel(obtain, 0);
                } else {
                    obtain.writeInt(0);
                }
                this._controlService.transact(1, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readInt();
            } catch (Exception e) {
                e.printStackTrace();
                StringBuilder sb = new StringBuilder("confielder_IControl.sendIR error: ");
                sb.append(e.toString());
                Log.e("UEI.SmartControl", sb.toString());
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }
        return 1;
    }




    public int stopIR() throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        if (this._controlService != null) {
            try {
                obtain.writeInterfaceToken(DESCRIPTOR);
                this._controlService.transact(4, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readInt();
            } catch (Exception e) {
                e.printStackTrace();
                StringBuilder sb = new StringBuilder("confielder_IControl.stopIR error: ");
                sb.append(e.toString());
                Log.e("UEI.SmartControl", sb.toString());
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }
        return 1;
    }


    public confielder_Device[] getDevices() throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        confielder_Device[] deviceArr = null;
        if (this._controlService == null) {
            return deviceArr;
        }
        try {
            obtain.writeInterfaceToken(DESCRIPTOR);
            this._controlService.transact(8, obtain, obtain2, 0);
            obtain2.readException();
            confielder_Device[] deviceArr2 = (confielder_Device[]) obtain2.createTypedArray(confielder_Device.CREATOR);
            obtain2.recycle();
            obtain.recycle();
            return deviceArr2;
        } catch (Exception e) {
            e.printStackTrace();
            StringBuilder sb = new StringBuilder("confielder_IControl.getDevices error: ");
            sb.append(e.toString());
            Log.e("UEI.SmartControl", sb.toString());
            obtain2.recycle();
            obtain.recycle();
            return deviceArr;
        } catch (Throwable th) {
            obtain2.recycle();
            obtain.recycle();
            throw th;
        }
    }


    public void registerCallback(confielder_IControlCallback iControlCallback) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        if (this._controlService != null) {
            try {
                obtain.writeInterfaceToken(DESCRIPTOR);
                obtain.writeStrongBinder(iControlCallback != null ? iControlCallback.asBinder() : null);
                this._controlService.transact(11, obtain, obtain2, 0);
                obtain2.readException();
            } catch (Exception e) {
                e.printStackTrace();
                StringBuilder sb = new StringBuilder("confielder_IControl.registerCallback error: ");
                sb.append(e.toString());
                Log.e("UEI.SmartControl", sb.toString());
            } catch (Throwable th) {
                obtain2.recycle();
                obtain.recycle();
                throw th;
            }
            obtain2.recycle();
            obtain.recycle();
        }
    }


    public int getLastResultcode() throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        if (this._controlService != null) {
            try {
                obtain.writeInterfaceToken(DESCRIPTOR);
                this._controlService.transact(13, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readInt();
            } catch (Exception e) {
                e.printStackTrace();
                StringBuilder sb = new StringBuilder("confielder_IControl.getLastResultcode error: ");
                sb.append(e.toString());
                Log.e("UEI.SmartControl", sb.toString());
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }
        return 1;
    }



    public int sendIRPattern(int i, int[] iArr) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        if (this._controlService != null) {
            try {
                obtain.writeInterfaceToken(DESCRIPTOR);
                obtain.writeInt(i);
                obtain.writeIntArray(iArr);
                this._controlService.transact(16, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readInt();
            } catch (Exception e) {
                e.printStackTrace();
                StringBuilder sb = new StringBuilder("confielder_IControl.sendIR error: ");
                sb.append(e.toString());
                Log.e("UEI.SmartControl", sb.toString());
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }
        return 1;
    }

}
