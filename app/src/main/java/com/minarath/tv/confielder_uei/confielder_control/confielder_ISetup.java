package com.minarath.tv.confielder_uei.confielder_control;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;

public class confielder_ISetup {
    public static final String DESCRIPTOR = "com.uei.control.confielder_ISetup";

    private IBinder _setupService = null;

    public confielder_ISetup(IBinder iBinder) {
        this._setupService = iBinder;
    }

    public boolean activateQuicksetService(String str) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        boolean z = false;
        if (this._setupService != null) {
            try {
                obtain.writeInterfaceToken(DESCRIPTOR);
                obtain.writeString(str);
                this._setupService.transact(1, obtain, obtain2, 0);
                obtain2.readException();
                if (obtain2.readInt() != 0) {
                    z = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
                StringBuilder sb = new StringBuilder("confielder_ISetup.activateQuicksetService error: ");
                sb.append(e.toString());
                Log.e("UEI.SmartControl", sb.toString());
            } catch (Throwable th) {
                obtain2.recycle();
                obtain.recycle();
                throw th;
            }
            obtain2.recycle();
            obtain.recycle();
        }
        return z;
    }



    public long getSession() throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        if (this._setupService != null) {
            try {
                obtain.writeInterfaceToken(DESCRIPTOR);
                this._setupService.transact(4, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readLong();
            } catch (Exception e) {
                e.printStackTrace();
                StringBuilder sb = new StringBuilder("confielder_ISetup.getSession error: ");
                sb.append(e.toString());
                Log.e("UEI.SmartControl", sb.toString());
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }
        return -1;
    }



    public int getLastResultcode() throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        if (this._setupService != null) {
            try {
                obtain.writeInterfaceToken(DESCRIPTOR);
                this._setupService.transact(22, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readInt();
            } catch (Exception e) {
                e.printStackTrace();
                StringBuilder sb = new StringBuilder("confielder_ISetup.getLastResultcode error: ");
                sb.append(e.toString());
                Log.e("UEI.SmartControl", sb.toString());
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }
        return 1;
    }


    public int validateSession(long j) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        if (this._setupService != null) {
            try {
                obtain.writeInterfaceToken(DESCRIPTOR);
                obtain.writeLong(j);
                this._setupService.transact(24, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readInt();
            } catch (Exception e) {
                e.printStackTrace();
                StringBuilder sb = new StringBuilder("confielder_ISetup.validateSession error: ");
                sb.append(e.toString());
                Log.e("UEI.SmartControl", sb.toString());
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }
        return 1;
    }


    public void registerSetupReadyCallback(confielder_ISetupReadyCallback iSetupReadyCallback) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        if (this._setupService != null) {
            try {
                obtain.writeInterfaceToken(DESCRIPTOR);
                obtain.writeStrongBinder(iSetupReadyCallback != null ? iSetupReadyCallback.asBinder() : null);
                this._setupService.transact(26, obtain, obtain2, 0);
                obtain2.readException();
            } catch (Exception e) {
                e.printStackTrace();
                StringBuilder sb = new StringBuilder("confielder_ISetup.registerSetupReadyCallback error: ");
                sb.append(e.toString());
                Log.e("UEI.SmartControl", sb.toString());
            } catch (Throwable th) {
                obtain2.recycle();
                obtain.recycle();
                throw th;
            }
            obtain2.recycle();
            obtain.recycle();
        }
    }

    public void unregisterSetupReadyCallback(confielder_ISetupReadyCallback iSetupReadyCallback) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        if (this._setupService != null) {
            try {
                obtain.writeInterfaceToken(DESCRIPTOR);
                obtain.writeStrongBinder(iSetupReadyCallback != null ? iSetupReadyCallback.asBinder() : null);
                this._setupService.transact(27, obtain, obtain2, 0);
                obtain2.readException();
            } catch (Exception e) {
                e.printStackTrace();
                StringBuilder sb = new StringBuilder("confielder_ISetup.unregisterSetupReadyCallback error: ");
                sb.append(e.toString());
                Log.e("UEI.SmartControl", sb.toString());
            } catch (Throwable th) {
                obtain2.recycle();
                obtain.recycle();
                throw th;
            }
            obtain2.recycle();
            obtain.recycle();
        }
    }


    public void registerLearnIRStatusCallback(confielder_ILearnIRStatusCallback iLearnIRStatusCallback) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        if (this._setupService != null) {
            try {
                obtain.writeInterfaceToken(DESCRIPTOR);
                obtain.writeStrongBinder(iLearnIRStatusCallback != null ? iLearnIRStatusCallback.asBinder() : null);
                this._setupService.transact(46, obtain, obtain2, 0);
                obtain2.readException();
            } catch (Exception e) {
                e.printStackTrace();
                StringBuilder sb = new StringBuilder("confielder_ISetup.registerLearnIRStatusCallback error: ");
                sb.append(e.toString());
                Log.e("UEI.SmartControl", sb.toString());
            } catch (Throwable th) {
                obtain2.recycle();
                obtain.recycle();
                throw th;
            }
            obtain2.recycle();
            obtain.recycle();
        }
    }


}
