package com.minarath.tv.confielder_uei.confielder_control;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class confielder_Device implements Parcelable {
    public static final Creator<confielder_Device> CREATOR = new Creator<confielder_Device>() {
        public confielder_Device createFromParcel(Parcel parcel) {
            return new confielder_Device(parcel, null);
        }

        public confielder_Device[] newArray(int i) {
            return new confielder_Device[i];
        }
    };
    public String Brand;
    public String DeviceTypeName;
    public int[] Functions;
    public int Id;
    public List<confielder_IRFunction> KeyFunctions;
    public String Model;
    public String Name;
    public String Tag;

    public int describeContents() {
        return 0;
    }

    public confielder_Device() {
        this.Name = "";
        this.Brand = "";
        this.Model = "";
        this.DeviceTypeName = "";
        this.Id = 0;
        this.Functions = null;
        this.Tag = "";
        this.KeyFunctions = new ArrayList();
    }

    private confielder_Device(Parcel parcel) {
        this.Name = "";
        this.Brand = "";
        this.Model = "";
        this.DeviceTypeName = "";
        this.Id = 0;
        this.Functions = null;
        this.Tag = "";
        this.KeyFunctions = new ArrayList();
        readFromParcel(parcel);
    }


    public confielder_Device(String name, String brand, String model, String deviceTypeName, int id, int[] functions, String tag, List<confielder_IRFunction> keyFunctions) {
        Brand = brand;
        DeviceTypeName = deviceTypeName;
        Functions = functions;
        Id = id;
        KeyFunctions = keyFunctions;
        Model = model;
        Name = name;
        Tag = tag;
    }

   confielder_Device(Parcel parcel, confielder_Device device) {
        this(parcel);
    }

    public void writeToParcel(Parcel parcel, int i) {
        try {
            parcel.writeInt(this.Id);
            parcel.writeString(this.Name);
            if (this.Brand == null) {
                this.Brand = "";
            }
            parcel.writeString(this.Brand);
            if (this.Model == null) {
                this.Model = "";
            }
            parcel.writeString(this.Model);
            if (this.DeviceTypeName == null) {
                this.DeviceTypeName = "";
            }
            parcel.writeString(this.DeviceTypeName);
            if (this.KeyFunctions == null || this.KeyFunctions.size() <= 0) {
                parcel.writeInt(0);
                return;
            }
            updateFunctionIds();
            parcel.writeInt(this.Functions.length);
            parcel.writeIntArray(this.Functions);
            confielder_IRFunction[] iRFunctionArr = new confielder_IRFunction[this.KeyFunctions.size()];
            this.KeyFunctions.toArray(iRFunctionArr);
            parcel.writeParcelableArray(iRFunctionArr, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void readFromParcel(Parcel parcel) {
        try {
            this.Id = parcel.readInt();
            this.Name = parcel.readString();
            this.Brand = parcel.readString();
            this.Model = parcel.readString();
            this.DeviceTypeName = parcel.readString();
            int readInt = parcel.readInt();
            resetKeyFunctions();
            if (readInt > 0) {
                this.Functions = new int[readInt];
                parcel.readIntArray(this.Functions);
                if (parcel.dataAvail() > 0) {
                    try {
                        Parcelable[] readParcelableArray = parcel.readParcelableArray(confielder_IRFunction.class.getClassLoader());
                        if (readParcelableArray != null) {
                            for (Parcelable parcelable : readParcelableArray) {
                                this.KeyFunctions.add((confielder_IRFunction) parcelable);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void updateFunctionIds() {
        this.Functions = new int[this.KeyFunctions.size()];
        for (int i = 0; i < this.Functions.length; i++) {
            this.Functions[i] = ((confielder_IRFunction) this.KeyFunctions.get(i)).Id;
        }
    }

    public String toString() {
        return this.Name;
    }

    private void resetKeyFunctions() {
        if (this.KeyFunctions == null) {
            this.KeyFunctions = new ArrayList();
        } else {
            this.KeyFunctions.clear();
        }
    }
}
