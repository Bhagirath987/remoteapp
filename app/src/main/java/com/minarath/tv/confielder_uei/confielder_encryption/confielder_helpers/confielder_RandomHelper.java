package com.minarath.tv.confielder_uei.confielder_encryption.confielder_helpers;

import com.minarath.tv.confielder_uei.confielder_encryption.confielder_random.confielder_RandomMwC;

public class confielder_RandomHelper {
    private int errorMs = 1000;
    private int[][] possible = null;
    private byte[][] possibleBytes = null;
    private int randomArraySize = 32;
    private byte[] randomBytes = null;
    private int[] randoms = null;
    private long timeMs = System.currentTimeMillis();
    private int windowMs = 5000;

    public long getTimeMs() {
        return this.timeMs;
    }

    public void setTimeMs(long j) {
        this.timeMs = j;
    }

    public int getWindowMs() {
        return this.windowMs;
    }

    public void setWindowMs(int i) {
        this.windowMs = i;
    }

    public int getErrorMs() {
        return this.errorMs;
    }

    public void setErrorMs(int i) {
        this.errorMs = i;
    }

    public int getRandomArraySize() {
        return this.randomArraySize;
    }

    public void setRandomArraySize(int i) {
        this.randomArraySize = i;
    }

    public int[] getRandoms() {
        return this.randoms;
    }

    public byte[] getRandomBytes() {
        return this.randomBytes;
    }

    public int[][] getPossible() {
        return this.possible;
    }

    public byte[][] getPossibleBytes() {
        return this.possibleBytes;
    }

    public long getSeed() {
        return this.timeMs / ((long) this.windowMs);
    }

    public long getHigherSeed() {
        return (this.timeMs + ((long) this.errorMs)) / ((long) this.windowMs);
    }

    public long getLowerSeed() {
        return (this.timeMs - ((long) this.errorMs)) / ((long) this.windowMs);
    }

    public void calculateRandoms() {
        this.randoms = createRandoms();
        this.randomBytes = convertToBytes(this.randoms);
    }



    private byte[] convertToBytes(int[] iArr) {
        byte[] bArr = new byte[(this.randomArraySize * 4)];
        for (int i = 0; i < this.randomArraySize; i++) {
            int i2 = i * 4;
            bArr[i2] = (byte) iArr[i];
            bArr[i2 + 1] = (byte) (iArr[i] >>> 8);
            bArr[i2 + 2] = (byte) (iArr[i] >>> 16);
            bArr[i2 + 3] = (byte) (iArr[i] >>> 24);
        }
        return bArr;
    }

    private int[] createRandoms() {
        return createRandoms(getSeed());
    }

    private int[] createRandoms(long j) {
        confielder_RandomMwC randomMwC = new confielder_RandomMwC(j);
        int[] iArr = new int[this.randomArraySize];
        for (int i = 0; i < this.randomArraySize; i++) {
            iArr[i] = randomMwC.nextInt();
        }
        return iArr;
    }


}
