package com.minarath.tv.confielder_Extra;

import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.ConsumerIrManager;
import android.util.Log;

import java.util.UUID;

@TargetApi(19)
public class confielder_ConsumerIrManagerBase extends confielder_ConsumerIrManagerCompat {
    private static final String TAG = "confielder_ConsumerIrManagerBase";
    private ConsumerIrManager mCIR;

    public confielder_ConsumerIrManagerBase(Context context) {
        super(context);
        this.mCIR = (ConsumerIrManager) context.getSystemService(Context.CONSUMER_IR_SERVICE);
    }


    public void transmit(int i, int[] iArr) {
        if (this.mCIR != null) {
            this.mCIR.transmit(i, iArr);
        }
    }

    public CarrierFrequencyRange[] getCarrierFrequencies() {
        if (this.mCIR == null) {
            return null;
        }
        ConsumerIrManager.CarrierFrequencyRange[] carrierFrequencies = this.mCIR.getCarrierFrequencies();
        CarrierFrequencyRange[] carrierFrequencyRangeArr = new CarrierFrequencyRange[carrierFrequencies.length];
        int length = carrierFrequencies.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            ConsumerIrManager.CarrierFrequencyRange carrierFrequencyRange = carrierFrequencies[i];
            int i3 = i2 + 1;
            carrierFrequencyRangeArr[i2] = new CarrierFrequencyRange(carrierFrequencyRange.getMinFrequency(), carrierFrequencyRange.getMaxFrequency());
            i++;
            i2 = i3;
        }
        return carrierFrequencyRangeArr;
    }

    public boolean hasIrEmitter() {
        if (this.mCIR != null) {
            return this.mCIR.hasIrEmitter();
        }
        return false;
    }

    public UUID learnIRCmd(int i) {
        Log.w(TAG, "learn IR command is not available on this device");
        return null;
    }

    public void start() {
        Log.w(TAG, "start() is not available on this device");
    }

    public void stop() {
        Log.w(TAG, "stop() is not available on this device");
    }

    public boolean isStarted() {
        return this.mCIR != null;
    }

    public UUID cancelCommand() {
        Log.w(TAG, "cancelCommand() is not available on this device");
        return null;
    }

    public UUID discardCommand(UUID uuid) {
        Log.w(TAG, "discardCommand(UUID) is not available on this device");
        return null;
    }
}
