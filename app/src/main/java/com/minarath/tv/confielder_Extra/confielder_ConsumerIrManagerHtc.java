package com.minarath.tv.confielder_Extra;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.minarath.tv.confielder_htc.confielder_circontrol.confielder_CIRControl;
import com.minarath.tv.confielder_htc.confielder_htcircontrol.confielder_HtcIrData;

import java.util.Arrays;
import java.util.UUID;

public class confielder_ConsumerIrManagerHtc extends confielder_ConsumerIrManagerCompat {
    private static final String TAG = "confielder_ConsumerIrManagerHtc";
    public Context mContext;
    public confielder_CIRControl mControl;
    Handler mHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message message) {
            int i = message.what;
            if (i != 6) {
                switch (i) {
                    case 1:
                        UUID uuid = (UUID) message.getData().getSerializable(confielder_CIRControl.KEY_RESULT_ID);
                        String str = confielder_ConsumerIrManagerHtc.TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Receive IR Returned UUID: ");
                        sb.append(uuid);
                        Log.i(str, sb.toString());
                        confielder_ConsumerIrManagerHtc.this.mLearntKey = (confielder_HtcIrData) message.getData().getSerializable(confielder_CIRControl.KEY_CMD_RESULT);
                        SharedPreferences sharedPreferences = confielder_ConsumerIrManagerHtc.this.mContext.getSharedPreferences(PREFERENCE_FILE_NAME, 0);
                        if (confielder_ConsumerIrManagerHtc.this.mLearntKey != null) {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("Repeat:");
                            sb2.append(confielder_ConsumerIrManagerHtc.this.mLearntKey.getRepeatCount());
                            sb2.append(" Freq:");
                            sb2.append(confielder_ConsumerIrManagerHtc.this.mLearntKey.getFrequency());
                            sb2.append(" Frame length:");
                            sb2.append(confielder_ConsumerIrManagerHtc.this.mLearntKey.getFrame().length);
                            sb2.append(" Frame= ");
                            sb2.append(Arrays.toString(confielder_ConsumerIrManagerHtc.this.mLearntKey.getFrame()));
                            String sb3 = sb2.toString();
                            Editor edit = sharedPreferences.edit();
                            edit.putInt(PREFERENCE_KEY_FREQUENCY, confielder_ConsumerIrManagerHtc.this.mLearntKey.getFrequency());
                            edit.putString(PREFERENCE_KEY_FRAME, Arrays.toString(confielder_ConsumerIrManagerHtc.this.mLearntKey.getFrame()));
                            edit.commit();
                            return;
                        }
                        int i2 = message.arg1;
                        if (i2 == 4) {
                            String str2 = "Learn IR Error: ERR_IO_ERROR";
                            return;
                        } else if (i2 == 20) {
                            String str3 = "Learn IR Error: ERR_LEARNING_TIMEOUT";
                            return;
                        } else if (i2 == 23) {
                            String str4 = "Learn IR Error: ERR_OUT_OF_FREQ";
                            return;
                        } else if (i2 != 25) {
                            String str5 = "";
                            return;
                        } else {
                            String str6 = "Learn IR Error: ERR_PULSE_ERROR";
                            return;
                        }
                    case 2:
                        UUID uuid2 = (UUID) message.getData().getSerializable(confielder_CIRControl.KEY_RESULT_ID);
                        String str7 = confielder_ConsumerIrManagerHtc.TAG;
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("Send IR Returned UUID: ");
                        sb4.append(uuid2);
                        Log.i(str7, sb4.toString());
                        int i3 = message.arg1;
                        if (i3 != 19) {
                            switch (i3) {
                                case 4:
                                    String str8 = "Send IR Error=ERR_IO_ERROR";
                                    return;
                                case 5:
                                    String str9 = "Send IR Error=ERR_CMD_DROPPED";
                                    return;
                                default:
                                    String str10 = "";
                                    return;
                            }
                        } else {
                            String str11 = "Send IR Error=ERR_INVALID_VALUE";
                            return;
                        }
                    default:
                        super.handleMessage(message);
                        return;
                }
            } else {
                int i4 = message.arg1;
                if (i4 == 4) {
                    String str12 = "Cancel Error: ERR_IO_ERROR";
                } else if (i4 != 21) {
                    String str13 = "";
                } else {
                    String str14 = "Cancel Error: ERR_CANCEL_FAIL";
                }
            }
        }
    };
    public confielder_HtcIrData mLearntKey;

    private class SendRunnable implements Runnable {
        private int[] frame;
        private int frequency;

        public SendRunnable(int i, int[] iArr) {
            this.frequency = i;
            this.frame = iArr;
        }

        public void run() {
            if (confielder_ConsumerIrManagerHtc.this.mLearntKey != null) {
                confielder_ConsumerIrManagerHtc.this.mControl.transmitIRCmd(confielder_ConsumerIrManagerHtc.this.mLearntKey, true);
                return;
            }
            try {
                confielder_ConsumerIrManagerHtc.this.mControl.transmitIRCmd(new confielder_HtcIrData(1, this.frequency, this.frame), false);
            } catch (IllegalArgumentException e) {
                StringBuilder sb = new StringBuilder();
                sb.append("new confielder_HtcIrData: ");
                sb.append(e);
                Log.e(confielder_ConsumerIrManagerHtc.TAG, sb.toString());
                throw e;
            }
        }
    }

    public confielder_ConsumerIrManagerHtc(Context context) {
        super(context);
        this.mContext = context;
        this.mControl = new confielder_CIRControl(context, this.mHandler);
        this.supportedAPIs &= 1;
    }

    public void transmit(int i, int[] iArr) {
        this.mHandler.post(new SendRunnable(i, iArr));
    }

    public CarrierFrequencyRange[] getCarrierFrequencies() {
        if (VERSION.SDK_INT < 19) {
            Log.i(TAG, "getCarrierFrequencies() is not available via the HTC CIR APIs");
        }
        return null;
    }


    public void start() {
        if (this.mControl != null) {
            this.mControl.start();
        } else {
            Log.w(TAG, "There is no CIRModule in this device , can't do start!");
        }
    }
}
