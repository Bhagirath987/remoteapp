package com.minarath.tv.confielder_Extra;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.widget.TextView;

public abstract class confielder_ConsumerIrManagerCompat {
    public static final int HTCSUPPORT = 1;
    public static final String PREFERENCE_FILE_NAME = "IR_Temp";
    public static final String PREFERENCE_KEY_FRAME = "IR_FRAME";
    public static final String PREFERENCE_KEY_FREQUENCY = "IR_FREQUENCY";
    protected Context mContext;
    protected int supportedAPIs = 0;
    protected TextView textView;

    public final class CarrierFrequencyRange {
        private int maxfreq;
        private int minfreq;

        public CarrierFrequencyRange(int i, int i2) {
            this.minfreq = i;
            this.maxfreq = i2;
        }

    }


    public abstract CarrierFrequencyRange[] getCarrierFrequencies();


    public abstract void start();


    public abstract void transmit(int i, int[] iArr);

    public static confielder_ConsumerIrManagerCompat createInstance(Context context) {
        if (hasPackage("com.htc.cirmodule", context)) {
            return new confielder_ConsumerIrManagerHtc(context);
        }
        return new confielder_ConsumerIrManagerBase(context);
    }

    public confielder_ConsumerIrManagerCompat(Context context) {
        this.mContext = context;
    }

    public static boolean hasPackage(String str, Context context) {
        for (ApplicationInfo applicationInfo : context.getPackageManager().getInstalledApplications(0)) {
            if (applicationInfo.packageName.equals(str)) {
                return true;
            }
        }
        return false;
    }

}
