package com.minarath.tv;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class Option_Activity extends AppCompatActivity {
    String acname;
    String brand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_option_);
        acname = getIntent().getStringExtra("acname");
        brand = getIntent().getStringExtra("brand");
    }

    public void IR(View view) {
        Intent intent;
        intent = new Intent(Option_Activity.this, confielder_TV_Activity.class);
        intent.putExtra("acname", acname);
        intent.putExtra("brand", brand);
        startActivity(intent);

    }

    public void Wifi(View view) {
        Intent intent = new Intent(this, ConnectActivity.class);
        intent.putExtra("DATA", "data");
        startActivity(intent);
    }
}