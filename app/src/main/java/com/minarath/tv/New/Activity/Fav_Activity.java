package com.minarath.tv.New.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.minarath.tv.New.Database.DB_Helper;
import com.minarath.tv.New.Helper.Utils;
import com.minarath.tv.New.Modal.Fav;
import com.minarath.tv.New.OnItemSelectionListener;
import com.minarath.tv.New.SubList_Adapter;
import com.minarath.tv.Option_Activity;
import com.minarath.tv.R;
import com.minarath.tv.confielder_Recycler_Adapter;
import com.minarath.tv.confielder_TV_Activity;
import com.minarath.tv.utils.GridSpacingItemDecoration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Fav_Activity extends AppCompatActivity {
    DB_Helper DBHelper;
    private ArrayList<String> list = new ArrayList<>();
    List<String> searchlist = new ArrayList<>();
    RecyclerView list_remote;
    private boolean isRefresh = true;
    private confielder_Recycler_Adapter adapter;
    public static ArrayList<Fav> list1 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_fav_);


        list_remote = findViewById(R.id.rc_FavList);
        list_remote.setHasFixedSize(true);

        list_remote.setLayoutManager(new GridLayoutManager(this, 2));
        int spanCount = 2; // 3 columns
        int spacing = 30; // 50px
        boolean includeEdge = true;
        list_remote.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));

        DBHelper = new DB_Helper(this);
        new Copy_database().execute();

    }

    private void fillList() {
        list.clear();
        list1 = DBHelper.getFav();
        for(int i = 0; i < list1.size(); i++) {
            list.add(list1.get(i).getName_());
        }
        Collections.sort(list);

    }

    class Copy_database extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            File database = Fav_Activity.this.getDatabasePath(DB_Helper.DBNAME);

            if(false == database.exists()) {
                DBHelper.getReadableDatabase();
                if(DBHelper.copyDataBase(Fav_Activity.this)) {
                    Log.e("Database Status", "Database Copied");
                }
                else {
                    Log.e("Database Status", "Database Copy error");
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            fillList();
            adapter = new confielder_Recycler_Adapter(Fav_Activity.this, list,
                    new OnItemSelectionListener() {
                        @Override
                        public void setPos(int pos) {
                        }

                        @Override
                        public void setSelected(String isSelected) {
                            Intent intent;
                            intent = new Intent(Fav_Activity.this, confielder_TV_Activity.class);
                            intent.putExtra("acname", isSelected);
                            startActivity(intent);
                            isRefresh = false;         }

                    });
            list_remote.setAdapter(adapter);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!isRefresh) {
            fillList();
            if(adapter != null) {
                adapter.setList(list);
                adapter.notifyDataSetChanged();
            }
            isRefresh = true;

        }
    }

    private void Search(String Enterd) {
        if(Enterd.equals("") && Enterd.equals(" ")) {
            adapter.setList((ArrayList<String>) list);
        }
        else {
            searchlist.clear();
            for(int i = 0; i < list.size(); i++) {
                if(list.get(i).toLowerCase().contains(Enterd.toLowerCase())) {
                    searchlist.add(list.get(i));
                }
            }
            adapter.setList((ArrayList<String>) searchlist);
        }
    }
}
