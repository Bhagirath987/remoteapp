package com.minarath.tv.New.Modal;


public class Projectors_sharp_Codes {
    IrCode Zoom;
    IrCode Resize;
    IrCode Volume_Up;
    IrCode Volume_Down;
    IrCode Power;
    IrCode Picture;
    IrCode Navigate_Up;
    IrCode Navigate_Left;
    IrCode Navigate_Right;
    IrCode Navigate_Down;
    IrCode Mute;
    IrCode Menu;
    IrCode Delay;
    IrCode AutoSync;
    IrCode Back;

    public IrCode getZoom() {
        return Zoom;
    }

    public void setZoom(IrCode zoom) {
        Zoom = zoom;
    }

    public IrCode getResize() {
        return Resize;
    }

    public void setResize(IrCode resize) {
        Resize = resize;
    }

    public IrCode getVolume_Up() {
        return Volume_Up;
    }

    public void setVolume_Up(IrCode volume_Up) {
        Volume_Up = volume_Up;
    }

    public IrCode getVolume_Down() {
        return Volume_Down;
    }

    public void setVolume_Down(IrCode volume_Down) {
        Volume_Down = volume_Down;
    }

    public IrCode getPower() {
        return Power;
    }

    public void setPower(IrCode power) {
        Power = power;
    }

    public IrCode getPicture() {
        return Picture;
    }

    public void setPicture(IrCode picture) {
        Picture = picture;
    }

    public IrCode getNavigate_Up() {
        return Navigate_Up;
    }

    public void setNavigate_Up(IrCode navigate_Up) {
        Navigate_Up = navigate_Up;
    }

    public IrCode getNavigate_Left() {
        return Navigate_Left;
    }

    public void setNavigate_Left(IrCode navigate_Left) {
        Navigate_Left = navigate_Left;
    }

    public IrCode getNavigate_Right() {
        return Navigate_Right;
    }

    public void setNavigate_Right(IrCode navigate_Right) {
        Navigate_Right = navigate_Right;
    }

    public IrCode getNavigate_Down() {
        return Navigate_Down;
    }

    public void setNavigate_Down(IrCode navigate_Down) {
        Navigate_Down = navigate_Down;
    }

    public IrCode getMute() {
        return Mute;
    }

    public void setMute(IrCode mute) {
        Mute = mute;
    }

    public IrCode getMenu() {
        return Menu;
    }

    public void setMenu(IrCode menu) {
        Menu = menu;
    }

    public IrCode getDelay() {
        return Delay;
    }

    public void setDelay(IrCode delay) {
        Delay = delay;
    }

    public IrCode getAutoSync() {
        return AutoSync;
    }

    public void setAutoSync(IrCode autoSync) {
        AutoSync = autoSync;
    }

    public IrCode getBack() {
        return Back;
    }

    public void setBack(IrCode back) {
        Back = back;
    }
}
