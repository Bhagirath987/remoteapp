package com.minarath.tv.New.Modal;


public class DVDPlayers_sharp_Codes {
    IrCode Stop;
    IrCode Select;
    IrCode Rewind;
    IrCode Record;
    IrCode Previous;
    IrCode Power;
    IrCode PopMenu;
    IrCode Play;
    IrCode Pause;
    IrCode Next;
    IrCode Navigate_Up;
    IrCode Navigate_Right;
    IrCode Navigate_Left;
    IrCode Navigate_Down;
    IrCode Mute;
    IrCode Menu;
    IrCode Fast_Forward;
    IrCode Exit;
    IrCode Display;
    IrCode Delay;

    public IrCode getStop() {
        return Stop;
    }

    public void setStop(IrCode stop) {
        Stop = stop;
    }

    public IrCode getSelect() {
        return Select;
    }

    public void setSelect(IrCode select) {
        Select = select;
    }

    public IrCode getRewind() {
        return Rewind;
    }

    public void setRewind(IrCode rewind) {
        Rewind = rewind;
    }

    public IrCode getRecord() {
        return Record;
    }

    public void setRecord(IrCode record) {
        Record = record;
    }

    public IrCode getPrevious() {
        return Previous;
    }

    public void setPrevious(IrCode previous) {
        Previous = previous;
    }

    public IrCode getPower() {
        return Power;
    }

    public void setPower(IrCode power) {
        Power = power;
    }

    public IrCode getPopMenu() {
        return PopMenu;
    }

    public void setPopMenu(IrCode popMenu) {
        PopMenu = popMenu;
    }

    public IrCode getPlay() {
        return Play;
    }

    public void setPlay(IrCode play) {
        Play = play;
    }

    public IrCode getPause() {
        return Pause;
    }

    public void setPause(IrCode pause) {
        Pause = pause;
    }

    public IrCode getNext() {
        return Next;
    }

    public void setNext(IrCode next) {
        Next = next;
    }

    public IrCode getNavigate_Up() {
        return Navigate_Up;
    }

    public void setNavigate_Up(IrCode navigate_Up) {
        Navigate_Up = navigate_Up;
    }

    public IrCode getNavigate_Right() {
        return Navigate_Right;
    }

    public void setNavigate_Right(IrCode navigate_Right) {
        Navigate_Right = navigate_Right;
    }

    public IrCode getNavigate_Left() {
        return Navigate_Left;
    }

    public void setNavigate_Left(IrCode navigate_Left) {
        Navigate_Left = navigate_Left;
    }

    public IrCode getNavigate_Down() {
        return Navigate_Down;
    }

    public void setNavigate_Down(IrCode navigate_Down) {
        Navigate_Down = navigate_Down;
    }

    public IrCode getMute() {
        return Mute;
    }

    public void setMute(IrCode mute) {
        Mute = mute;
    }

    public IrCode getMenu() {
        return Menu;
    }

    public void setMenu(IrCode menu) {
        Menu = menu;
    }

    public IrCode getFast_Forward() {
        return Fast_Forward;
    }

    public void setFast_Forward(IrCode fast_Forward) {
        Fast_Forward = fast_Forward;
    }

    public IrCode getExit() {
        return Exit;
    }

    public void setExit(IrCode exit) {
        Exit = exit;
    }

    public IrCode getDisplay() {
        return Display;
    }

    public void setDisplay(IrCode display) {
        Display = display;
    }

    public IrCode getDelay() {
        return Delay;
    }

    public void setDelay(IrCode delay) {
        Delay = delay;
    }
}
