package com.minarath.tv.New.Helper;

import android.content.Context;
import android.view.View;

import com.minarath.tv.New.Database.DB_Helper;

public class Utils {
    public static final String TV = "TV";

    public static void addToFav(DB_Helper db_helper, String fragment, String brand) {
        db_helper.fav_(fragment, brand);
    }

    public static void delToFav(DB_Helper db_helper, String fragment, String brand) {
        db_helper.del_rec(fragment, brand);
    }

    public static void setSize(Context context, View view, int width, int height, boolean b) {
        view.getLayoutParams().width = w(context, width);
        if(b) {
            view.getLayoutParams().height = h(context, height);
        }
        else {
            view.getLayoutParams().height = w(context, height);
        }
    }

    public static int w(Context context,int v) {
        return (context.getResources().getDisplayMetrics().widthPixels * v / 1080);
    }

    public static int h(Context context,int v) {
        return (context.getResources().getDisplayMetrics().widthPixels * v / 1920);
    }
}
