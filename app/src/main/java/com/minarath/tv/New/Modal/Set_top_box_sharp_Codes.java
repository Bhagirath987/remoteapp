package com.minarath.tv.New.Modal;


public class Set_top_box_sharp_Codes {
    IrCode zero;
    IrCode one;
    IrCode two;
    IrCode three;
    IrCode four;
    IrCode five;
    IrCode six;
    IrCode seven;
    IrCode eight;
    IrCode nine;
    IrCode Channel_Down;
    IrCode Channel_Up;
    IrCode Delay;
    IrCode Exit;
    IrCode Fast_Forward;
    IrCode Green;
    IrCode Guide;
    IrCode Menu;
    IrCode Mute;
    IrCode Navigate_Down;
    IrCode Navigate_Left;
    IrCode Navigate_Right;
    IrCode Navigate_Up;
    IrCode Next;
    IrCode Play;
    IrCode Pause;
    IrCode Power;
    IrCode Previous;
    IrCode Record;
    IrCode Red;
    IrCode Rewind;
    IrCode Return;
    IrCode Select;
    IrCode Stop;
    IrCode Volume_Down;
    IrCode Volume_Up;
    IrCode Yellow;

    public IrCode getZero() {
        return zero;
    }

    public void setZero(IrCode zero) {
        this.zero = zero;
    }

    public IrCode getOne() {
        return one;
    }

    public void setOne(IrCode one) {
        this.one = one;
    }

    public IrCode getTwo() {
        return two;
    }

    public void setTwo(IrCode two) {
        this.two = two;
    }

    public IrCode getThree() {
        return three;
    }

    public void setThree(IrCode three) {
        this.three = three;
    }

    public IrCode getFour() {
        return four;
    }

    public void setFour(IrCode four) {
        this.four = four;
    }

    public IrCode getFive() {
        return five;
    }

    public void setFive(IrCode five) {
        this.five = five;
    }

    public IrCode getSix() {
        return six;
    }

    public void setSix(IrCode six) {
        this.six = six;
    }

    public IrCode getSeven() {
        return seven;
    }

    public void setSeven(IrCode seven) {
        this.seven = seven;
    }

    public IrCode getEight() {
        return eight;
    }

    public void setEight(IrCode eight) {
        this.eight = eight;
    }

    public IrCode getNine() {
        return nine;
    }

    public void setNine(IrCode nine) {
        this.nine = nine;
    }

    public IrCode getChannel_Down() {
        return Channel_Down;
    }

    public void setChannel_Down(IrCode channel_Down) {
        Channel_Down = channel_Down;
    }

    public IrCode getChannel_Up() {
        return Channel_Up;
    }

    public void setChannel_Up(IrCode channel_Up) {
        Channel_Up = channel_Up;
    }

    public IrCode getDelay() {
        return Delay;
    }

    public void setDelay(IrCode delay) {
        Delay = delay;
    }

    public IrCode getExit() {
        return Exit;
    }

    public void setExit(IrCode exit) {
        Exit = exit;
    }

    public IrCode getFast_Forward() {
        return Fast_Forward;
    }

    public void setFast_Forward(IrCode fast_Forward) {
        Fast_Forward = fast_Forward;
    }

    public IrCode getGreen() {
        return Green;
    }

    public void setGreen(IrCode green) {
        Green = green;
    }

    public IrCode getGuide() {
        return Guide;
    }

    public void setGuide(IrCode guide) {
        Guide = guide;
    }

    public IrCode getMenu() {
        return Menu;
    }

    public void setMenu(IrCode menu) {
        Menu = menu;
    }

    public IrCode getMute() {
        return Mute;
    }

    public void setMute(IrCode mute) {
        Mute = mute;
    }

    public IrCode getNavigate_Down() {
        return Navigate_Down;
    }

    public void setNavigate_Down(IrCode navigate_Down) {
        Navigate_Down = navigate_Down;
    }

    public IrCode getNavigate_Left() {
        return Navigate_Left;
    }

    public void setNavigate_Left(IrCode navigate_Left) {
        Navigate_Left = navigate_Left;
    }

    public IrCode getNavigate_Right() {
        return Navigate_Right;
    }

    public void setNavigate_Right(IrCode navigate_Right) {
        Navigate_Right = navigate_Right;
    }

    public IrCode getNavigate_Up() {
        return Navigate_Up;
    }

    public void setNavigate_Up(IrCode navigate_Up) {
        Navigate_Up = navigate_Up;
    }

    public IrCode getNext() {
        return Next;
    }

    public void setNext(IrCode next) {
        Next = next;
    }

    public IrCode getPlay() {
        return Play;
    }

    public void setPlay(IrCode play) {
        Play = play;
    }

    public IrCode getPause() {
        return Pause;
    }

    public void setPause(IrCode pause) {
        Pause = pause;
    }

    public IrCode getPower() {
        return Power;
    }

    public void setPower(IrCode power) {
        Power = power;
    }

    public IrCode getPrevious() {
        return Previous;
    }

    public void setPrevious(IrCode previous) {
        Previous = previous;
    }

    public IrCode getRecord() {
        return Record;
    }

    public void setRecord(IrCode record) {
        Record = record;
    }

    public IrCode getRed() {
        return Red;
    }

    public void setRed(IrCode red) {
        Red = red;
    }

    public IrCode getRewind() {
        return Rewind;
    }

    public void setRewind(IrCode rewind) {
        Rewind = rewind;
    }

    public IrCode getReturn() {
        return Return;
    }

    public void setReturn(IrCode aReturn) {
        Return = aReturn;
    }

    public IrCode getSelect() {
        return Select;
    }

    public void setSelect(IrCode select) {
        Select = select;
    }

    public IrCode getStop() {
        return Stop;
    }

    public void setStop(IrCode stop) {
        Stop = stop;
    }

    public IrCode getVolume_Down() {
        return Volume_Down;
    }

    public void setVolume_Down(IrCode volume_Down) {
        Volume_Down = volume_Down;
    }

    public IrCode getVolume_Up() {
        return Volume_Up;
    }

    public void setVolume_Up(IrCode volume_Up) {
        Volume_Up = volume_Up;
    }

    public IrCode getYellow() {
        return Yellow;
    }

    public void setYellow(IrCode yellow) {
        Yellow = yellow;
    }
}