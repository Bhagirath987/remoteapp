package com.minarath.tv.New.Modal;

public class Fav {
    String name_;
    String device;

    public Fav(String name_, String device) {
        this.name_ = name_;
        this.device = device;
    }

    public String getName_() {
        return name_;
    }

    public void setName_(String name_) {
        this.name_ = name_;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}
