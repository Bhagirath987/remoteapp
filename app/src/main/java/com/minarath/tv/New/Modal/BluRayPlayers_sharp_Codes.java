package com.minarath.tv.New.Modal;


public class BluRayPlayers_sharp_Codes {
    IrCode zero;
    IrCode one;
    IrCode two;
    IrCode three;
    IrCode four;
    IrCode five;
    IrCode six;
    IrCode seven;
    IrCode eight;
    IrCode nine;
    IrCode Delay;
    IrCode Display;
    IrCode Exit;
    IrCode Fast_Forward;
    IrCode Green;
    IrCode Red;
    IrCode Yellow;
    IrCode Blue;
    IrCode Menu;
    IrCode Navigate_Up;
    IrCode Navigate_Down;
    IrCode Navigate_Right;
    IrCode Navigate_Left;
    IrCode Next;
    IrCode Pause;
    IrCode Play;
    IrCode Power;
    IrCode Previous;
    IrCode Rewind;
    IrCode Select;
    IrCode Stop;

    public IrCode getZero() {
        return zero;
    }

    public void setZero(IrCode zero) {
        this.zero = zero;
    }

    public IrCode getOne() {
        return one;
    }

    public void setOne(IrCode one) {
        this.one = one;
    }

    public IrCode getTwo() {
        return two;
    }

    public void setTwo(IrCode two) {
        this.two = two;
    }

    public IrCode getThree() {
        return three;
    }

    public void setThree(IrCode three) {
        this.three = three;
    }

    public IrCode getFour() {
        return four;
    }

    public void setFour(IrCode four) {
        this.four = four;
    }

    public IrCode getFive() {
        return five;
    }

    public void setFive(IrCode five) {
        this.five = five;
    }

    public IrCode getSix() {
        return six;
    }

    public void setSix(IrCode six) {
        this.six = six;
    }

    public IrCode getSeven() {
        return seven;
    }

    public void setSeven(IrCode seven) {
        this.seven = seven;
    }

    public IrCode getEight() {
        return eight;
    }

    public void setEight(IrCode eight) {
        this.eight = eight;
    }

    public IrCode getNine() {
        return nine;
    }

    public void setNine(IrCode nine) {
        this.nine = nine;
    }

    public IrCode getDelay() {
        return Delay;
    }

    public void setDelay(IrCode delay) {
        Delay = delay;
    }

    public IrCode getDisplay() {
        return Display;
    }

    public void setDisplay(IrCode display) {
        Display = display;
    }

    public IrCode getExit() {
        return Exit;
    }

    public void setExit(IrCode exit) {
        Exit = exit;
    }

    public IrCode getFast_Forward() {
        return Fast_Forward;
    }

    public void setFast_Forward(IrCode fast_Forward) {
        Fast_Forward = fast_Forward;
    }

    public IrCode getGreen() {
        return Green;
    }

    public void setGreen(IrCode green) {
        Green = green;
    }

    public IrCode getRed() {
        return Red;
    }

    public void setRed(IrCode red) {
        Red = red;
    }

    public IrCode getYellow() {
        return Yellow;
    }

    public void setYellow(IrCode yellow) {
        Yellow = yellow;
    }

    public IrCode getBlue() {
        return Blue;
    }

    public void setBlue(IrCode blue) {
        Blue = blue;
    }

    public IrCode getMenu() {
        return Menu;
    }

    public void setMenu(IrCode menu) {
        Menu = menu;
    }

    public IrCode getNavigate_Up() {
        return Navigate_Up;
    }

    public void setNavigate_Up(IrCode navigate_Up) {
        Navigate_Up = navigate_Up;
    }

    public IrCode getNavigate_Down() {
        return Navigate_Down;
    }

    public void setNavigate_Down(IrCode navigate_Down) {
        Navigate_Down = navigate_Down;
    }

    public IrCode getNavigate_Right() {
        return Navigate_Right;
    }

    public void setNavigate_Right(IrCode navigate_Right) {
        Navigate_Right = navigate_Right;
    }

    public IrCode getNavigate_Left() {
        return Navigate_Left;
    }

    public void setNavigate_Left(IrCode navigate_Left) {
        Navigate_Left = navigate_Left;
    }

    public IrCode getNext() {
        return Next;
    }

    public void setNext(IrCode next) {
        Next = next;
    }

    public IrCode getPause() {
        return Pause;
    }

    public void setPause(IrCode pause) {
        Pause = pause;
    }

    public IrCode getPlay() {
        return Play;
    }

    public void setPlay(IrCode play) {
        Play = play;
    }

    public IrCode getPower() {
        return Power;
    }

    public void setPower(IrCode power) {
        Power = power;
    }

    public IrCode getPrevious() {
        return Previous;
    }

    public void setPrevious(IrCode previous) {
        Previous = previous;
    }

    public IrCode getRewind() {
        return Rewind;
    }

    public void setRewind(IrCode rewind) {
        Rewind = rewind;
    }

    public IrCode getSelect() {
        return Select;
    }

    public void setSelect(IrCode select) {
        Select = select;
    }

    public IrCode getStop() {
        return Stop;
    }

    public void setStop(IrCode stop) {
        Stop = stop;
    }
}
