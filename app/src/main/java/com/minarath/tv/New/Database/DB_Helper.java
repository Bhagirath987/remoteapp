package com.minarath.tv.New.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.minarath.tv.New.Modal.BluRayPlayers_sharp_Codes;
import com.minarath.tv.New.Modal.DVDPlayers_sharp_Codes;
import com.minarath.tv.New.Modal.Fav;
import com.minarath.tv.New.Modal.IrCode;
import com.minarath.tv.New.Modal.Projectors_sharp_Codes;
import com.minarath.tv.New.Modal.RemoteCodes;
import com.minarath.tv.New.Modal.Set_top_box_sharp_Codes;
import com.minarath.tv.New.Modal.TV_sharp_Codes;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class DB_Helper extends SQLiteOpenHelper {

    public static final String DBNAME = "TV";

    public static String DBLOCATION;
    //    public static final String DBLOCATION = "/data/data/com.sticker.remotephilips/databases/";
    private Context context;
    private SQLiteDatabase mDatabase;

    public DB_Helper(Context context) {
        super(context, DBNAME, null, 1);
        DBLOCATION = context.getApplicationInfo().dataDir + "/databases/";
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void openDatabase() {
        String dbpath = context.getDatabasePath(DBNAME).getPath();
        if(mDatabase != null && mDatabase.isOpen()) {
            return;
        }
        mDatabase = SQLiteDatabase.openDatabase(dbpath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db.disableWriteAheadLogging();
    }

    public void closeDatabase() {
        if(mDatabase != null) {
            mDatabase.close();
        }
    }

    public List<RemoteCodes> getCode(String brand, String device) {
        List<RemoteCodes> list = new ArrayList<>();
        openDatabase();
        String str =
                "SELECT * FROM remote WHERE brand='" + brand + "' AND device = '" + device + "'";
        Log.e("QUERY", str);
        Cursor cursor = mDatabase.rawQuery(str, null);
        Log.e("DATABASE", "LIST PICKED" + cursor.getCount());
        while(cursor.moveToNext()) {
            list.add(new RemoteCodes(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                    cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor
                    .getString(6)));
        }
        return list;

    }

    public List<String> getBrand() {
        List<String> list = new ArrayList<>();
        openDatabase();
        String str = "SELECT * FROM remote";
        Cursor cursor = mDatabase.rawQuery(str, null);
        while(cursor.moveToNext()) {
            String brand=cursor.getString(cursor.getColumnIndex("brand"));
            if(list.size()==0) {
                list.add(brand);
            }
            if(!list.contains(brand)) {
                list.add(brand);
            }
        }
        return list;

    }

    public boolean copyDataBase(Context context) {
        try {
            InputStream inputStream = context.getAssets().open(DB_Helper.DBNAME);
            String outfileame = DB_Helper.DBLOCATION + DB_Helper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outfileame);
            byte[] buf = new byte[1024];
            int lenth = 0;
            while((lenth = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, lenth);
            }
            outputStream.flush();
            outputStream.close();
            return true;
        } catch (Exception e) {
            e.getStackTrace();
            return false;
        }
    }

    public BluRayPlayers_sharp_Codes getAllBlueProcess(String brand, String fragment) {
        BluRayPlayers_sharp_Codes list = new BluRayPlayers_sharp_Codes();

        list.setZero(getNo(brand, fragment, "0"));
        list.setOne(getNo(brand, fragment, "1"));
        list.setTwo(getNo(brand, fragment, "2"));
        list.setThree(getNo(brand, fragment, "3"));
        list.setFour(getNo(brand, fragment, "4"));
        list.setFive(getNo(brand, fragment, "5"));
        list.setSix(getNo(brand, fragment, "6"));
        list.setSeven(getNo(brand, fragment, "7"));
        list.setEight(getNo(brand, fragment, "8"));
        list.setNine(getNo(brand, fragment, "9"));
        list.setDelay(getNo(brand, fragment, "Delay"));
        list.setDisplay(getNo(brand, fragment, "Display"));
        list.setExit(getNo(brand, fragment, "Exit"));
        list.setFast_Forward(getNo(brand, fragment, "Fast_Forward"));
        list.setGreen(getNo(brand, fragment, "Green"));
        list.setRed(getNo(brand, fragment, "Red"));
        list.setYellow(getNo(brand, fragment, "Yellow"));
        list.setBlue(getNo(brand, fragment, "Blue"));
        list.setMenu(getNo(brand, fragment, "Menu"));
        list.setNavigate_Down(getNo(brand, fragment, "Navigate_Down"));
        list.setNavigate_Left(getNo(brand, fragment, "Navigate_Left"));
        list.setNavigate_Right(getNo(brand, fragment, "Navigate_Right"));
        list.setNavigate_Up(getNo(brand, fragment, "Navigate_Up"));
        list.setNext(getNo(brand, fragment, "Next"));
        list.setPause(getNo(brand, fragment, "Pause"));
        list.setPlay(getNo(brand, fragment, "Play"));
        list.setPower(getNo(brand, fragment, "Power"));
        list.setPrevious(getNo(brand, fragment, "Previous"));
        list.setRewind(getNo(brand, fragment, "Rewind"));
        list.setSelect(getNo(brand, fragment, "Select"));
        list.setStop(getNo(brand, fragment, "Stop"));
        return list;
    }

    public DVDPlayers_sharp_Codes getAllDVDProcess(String brand, String fragment) {
        DVDPlayers_sharp_Codes list = new DVDPlayers_sharp_Codes();

        list.setStop(getNo(brand, fragment, "Stop"));
        list.setSelect(getNo(brand, fragment, "Select"));
        list.setRewind(getNo(brand, fragment, "Rewind"));
        list.setRecord(getNo(brand, fragment, "Record"));
        list.setPrevious(getNo(brand, fragment, "Previous"));
        list.setPower(getNo(brand, fragment, "Power"));
        list.setPopMenu(getNo(brand, fragment, "PopMenu"));
        list.setPlay(getNo(brand, fragment, "Play"));
        list.setPause(getNo(brand, fragment, "Pause"));
        list.setNext(getNo(brand, fragment, "Next"));
        list.setNavigate_Up(getNo(brand, fragment, "Navigate_Up"));
        list.setNavigate_Right(getNo(brand, fragment, "Navigate_Right"));
        list.setNavigate_Left(getNo(brand, fragment, "Navigate_Left"));
        list.setNavigate_Down(getNo(brand, fragment, "Navigate_Down"));
        list.setMute(getNo(brand, fragment, "Mute"));
        list.setMenu(getNo(brand, fragment, "Menu"));
        list.setFast_Forward(getNo(brand, fragment, "Fast_Forward"));
        list.setExit(getNo(brand, fragment, "Exit"));
        list.setDisplay(getNo(brand, fragment, "Display"));
        list.setDelay(getNo(brand, fragment, "Delay"));
        return list;
    }

    public Set_top_box_sharp_Codes getAllSet_top_boxProcess(String brand, String fragment) {
        Set_top_box_sharp_Codes list = new Set_top_box_sharp_Codes();
        list.setZero(getNo(brand, fragment, "0"));
        list.setOne(getNo(brand, fragment, "1"));
        list.setTwo(getNo(brand, fragment, "2"));
        list.setThree(getNo(brand, fragment, "3"));
        list.setFour(getNo(brand, fragment, "4"));
        list.setFive(getNo(brand, fragment, "5"));
        list.setSix(getNo(brand, fragment, "6"));
        list.setSeven(getNo(brand, fragment, "7"));
        list.setEight(getNo(brand, fragment, "8"));
        list.setNine(getNo(brand, fragment, "9"));
        list.setChannel_Down(getNo(brand, fragment, "Channel_Down"));
        list.setChannel_Up(getNo(brand, fragment, "Channel_Up"));
        list.setDelay(getNo(brand, fragment, "Delay"));
        list.setExit(getNo(brand, fragment, "Exit"));
        list.setFast_Forward(getNo(brand, fragment, "Fast_Forward"));
        list.setGreen(getNo(brand, fragment, "Green"));
        list.setGuide(getNo(brand, fragment, "Guide"));
        list.setMenu(getNo(brand, fragment, "Menu"));
        list.setMute(getNo(brand, fragment, "Mute"));
        list.setNavigate_Down(getNo(brand, fragment, "Navigate_Down"));
        list.setNavigate_Left(getNo(brand, fragment, "Navigate_Left"));
        list.setNavigate_Right(getNo(brand, fragment, "Navigate_Right"));
        list.setNavigate_Up(getNo(brand, fragment, "Navigate_Up"));
        list.setNext(getNo(brand, fragment, "Next"));
        list.setPlay(getNo(brand, fragment, "Play"));
        list.setPause(getNo(brand, fragment, "Pause"));
        list.setPower(getNo(brand, fragment, "Power"));
        list.setPrevious(getNo(brand, fragment, "Previous"));
        list.setRecord(getNo(brand, fragment, "Record"));
        list.setRed(getNo(brand, fragment, "Red"));
        list.setRewind(getNo(brand, fragment, "Rewind"));
        list.setReturn(getNo(brand, fragment, "Return"));
        list.setSelect(getNo(brand, fragment, "Select"));
        list.setStop(getNo(brand, fragment, "Stop"));
        list.setVolume_Down(getNo(brand, fragment, "Volume_Down"));
        list.setVolume_Up(getNo(brand, fragment, "Volume_Up"));
        list.setYellow(getNo(brand, fragment, "Yellow"));
        return list;
    }

    public TV_sharp_Codes getAllProcess(String brand, String fragment) {
        TV_sharp_Codes list = new TV_sharp_Codes();
        list.setOne(getNo(brand, fragment, "1"));
        list.setTwo(getNo(brand, fragment, "2"));
        list.setExit(getNo(brand, fragment, "exit"));
        list.setThree(getNo(brand, fragment, "3"));
        list.setFour(getNo(brand, fragment, "4"));
        list.setFive(getNo(brand, fragment, "5"));
        list.setSix(getNo(brand, fragment, "6"));
        list.setSeven(getNo(brand, fragment, "7"));
        list.setEight(getNo(brand, fragment, "8"));
        list.setNine(getNo(brand, fragment, "9"));
        list.setZero(getNo(brand, fragment, "0"));
        list.setBack(getNo(brand, fragment, "Back"));
        list.setBlue(getNo(brand, fragment, "Blue"));
        list.setCS(getNo(brand, fragment, "CS"));
        list.setCard(getNo(brand, fragment, "Card"));
        list.setChannel_Down(getNo(brand, fragment, "Channel_Down"));
        list.setChannel_Up(getNo(brand, fragment, "Channel_Up"));
        list.setDIGITAL(getNo(brand, fragment, "DIGITAL"));
        list.setDTV(getNo(brand, fragment, "DTV"));
        list.setDelay(getNo(brand, fragment, "Delay"));
        list.setDisplay(getNo(brand, fragment, "Display"));
        list.setEnter(getNo(brand, fragment, "Enter"));
        list.setExit(getNo(brand, fragment, "Exit"));
        list.setFast_Forward(getNo(brand, fragment, "Fast_Forward"));
        list.setFreeze(getNo(brand, fragment, "Freeze"));
        list.setGreen(getNo(brand, fragment, "Green"));
        list.setInput(getNo(brand, fragment, "Input"));
        list.setLast(getNo(brand, fragment, "Last"));
        list.setMenu(getNo(brand, fragment, "Menu"));
        list.setMute(getNo(brand, fragment, "Mute"));
        list.setNavigate_Down(getNo(brand, fragment, "Navigate_Down"));
        list.setNavigate_Left(getNo(brand, fragment, "Navigate_Left"));
        list.setNavigate_Right(getNo(brand, fragment, "Navigate_Right"));
        list.setNavigate_Up(getNo(brand, fragment, "Navigate_Up"));
        list.setPlay(getNo(brand, fragment, "Play"));
        list.setPause(getNo(brand, fragment, "Pause"));
        list.setPower(getNo(brand, fragment, "Power"));
        list.setRADIO(getNo(brand, fragment, "RADIO"));
        list.setRed(getNo(brand, fragment, "Red"));
        list.setRewind(getNo(brand, fragment, "Rewind"));
        list.setSelect(getNo(brand, fragment, "Select"));
        list.setSleep(getNo(brand, fragment, "Sleep"));
        list.setStop(getNo(brand, fragment, "Stop"));
        list.setUSB(getNo(brand, fragment, "USB"));
        list.setVolume_Down(getNo(brand, fragment, "Volume_Down"));
        list.setVolume_Up(getNo(brand, fragment, "Volume_Up"));
        list.setYellow(getNo(brand, fragment, "Yellow"));
        return list;
    }

    public IrCode getAll(String brand, String fragment, String s) {
        IrCode list = null;
        openDatabase();
        String str;
        if(s.equals("Blue")) {
            str = "SELECT * FROM remote Where fragment ='" + fragment + "' AND brand='" +
                    brand + "' AND button_fragment LIKE '%" + s + "%'";
        }
        else if(s.equals("Play")) {
            str = "SELECT * FROM remote Where fragment ='" + fragment + "' AND brand='" +
                    brand + "' AND button_fragment LIKE '" + s + "%'";
        }
        else {
            str = "SELECT * FROM remote Where fragment ='" + fragment + "' AND brand='" +
                    brand + "' AND button_fragment LIKE '%" + s + "'";
        }
        //        button_fragment "
        Cursor cursor = mDatabase.rawQuery(str, null);
        while(cursor.moveToNext()) {
            list = new IrCode(cursor.getString(5), cursor.getString(6));
            Log.e("Hello", ">>>>" + cursor.getString(5) + "========" + cursor.getString(6));
            break;
        }
        return list;
    }

    public Projectors_sharp_Codes getAllProjectorsProcess(String brand, String fragment) {
        Projectors_sharp_Codes list = new Projectors_sharp_Codes();

        list.setZoom(getNo(brand, fragment, "Zoom"));
        list.setResize(getNo(brand, fragment, "Resize"));
        list.setVolume_Up(getNo(brand, fragment, "Volume_Up"));
        list.setVolume_Down(getNo(brand, fragment, "Volume_Down"));
        list.setPower(getNo(brand, fragment, "Power"));
        list.setPicture(getNo(brand, fragment, "Picture"));
        list.setNavigate_Up(getNo(brand, fragment, "Navigate_Up"));
        list.setNavigate_Left(getNo(brand, fragment, "Navigate_Left"));
        list.setNavigate_Right(getNo(brand, fragment, "Navigate_Right"));
        list.setNavigate_Down(getNo(brand, fragment, "Navigate_Down"));
        list.setMute(getNo(brand, fragment, "Mute"));
        list.setMenu(getNo(brand, fragment, "Menu"));
        list.setDelay(getNo(brand, fragment, "Delay"));
        list.setAutoSync(getNo(brand, fragment, "AutoSync"));
        list.setBack(getNo(brand, fragment, "Back"));
        return list;
    }

    public IrCode getNo(String brand, String fragment, String s) {
        IrCode list = null;
        openDatabase();
        String str;
        str = "SELECT * FROM remote Where fragment ='" + fragment + "' AND brand='" +
                brand + "' AND button_fragment LIKE '" + s + "'";
        //        button_fragment "
        Cursor cursor = mDatabase.rawQuery(str, null);
        while(cursor.moveToNext()) {
            list = new IrCode(cursor.getString(5), cursor.getString(6));
            Log.e("Hello", ">>>>" + cursor.getString(5) + "========" + cursor.getString(6));
            break;
        }
        return list;
    }

    public long fav_(String fragment, String brand) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        if(isAvailable(fragment)) {
            return 1;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("sub_type", fragment);
        contentValues.put("brand", brand);
        long is_Done = sqLiteDatabase.insert("fav_Table", null, contentValues);
        Toast.makeText(context, "Added to Fav Succesfully!!!....", Toast.LENGTH_SHORT).show();
        return is_Done;
    }

    public int del_rec(String fragment, String brand) {
        openDatabase();
        int i = mDatabase.delete("fav_Table",
                "sub_type='" + fragment + "' and brand='" + brand + "'", null);
        Toast.makeText(context, "Deleted Succesfully!!!....", Toast.LENGTH_SHORT).show();
        return i;
    }

    public boolean isFav(String fragment) {
        if(isAvailable(fragment)) {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean isAvailable(String fragment) {
        openDatabase();
        String str = "SELECT * FROM fav_Table where sub_type='" + fragment + "'";
        Log.e("QUERY", str);
        Cursor cursor = mDatabase.rawQuery(str, null);
        if(cursor.moveToNext()) {
            return true;
        }
        else {
            return false;
        }
    }

    public ArrayList<Fav> getFav() {
        openDatabase();
        ArrayList<Fav> list = new ArrayList<>();
        String str = "SELECT * FROM fav_Table";
        Log.e("QUERY", str);
        Cursor cursor = mDatabase.rawQuery(str, null);
        Log.e("DATABASE", "LIST PICKED" + cursor.getCount());
        while(cursor.moveToNext()) {
            list.add(new Fav(cursor.getString(0), cursor.getString(1)));
        }
        return list;
    }


}