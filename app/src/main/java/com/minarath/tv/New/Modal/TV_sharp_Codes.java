package com.minarath.tv.New.Modal;


public class TV_sharp_Codes {
    IrCode One;
    IrCode Two;
    IrCode exit;
    IrCode Three;
    IrCode Four;
    IrCode Five;
    IrCode Six;
    IrCode Seven;
    IrCode Eight;
    IrCode Nine;
    IrCode Zero;
    IrCode Back;
    IrCode Blue;
    IrCode CS;
    IrCode Card;
    IrCode Channel_Down;
    IrCode Channel_Up;
    IrCode DIGITAL;
    IrCode DTV;
    IrCode Delay;
    IrCode Display;
    IrCode Enter;
    IrCode Exit;
    IrCode Fast_Forward;
    IrCode Freeze;
    IrCode Green;
    IrCode Input;
    IrCode Last;
    IrCode Menu;
    IrCode Mute;
    IrCode Navigate_Down;
    IrCode Navigate_Left;
    IrCode Navigate_Right;
    IrCode Navigate_Up;
    IrCode Play;
    IrCode Pause;
    IrCode Power;
    IrCode RADIO;
    IrCode Red;
    IrCode Rewind;
    IrCode Select;
    IrCode Sleep;
    IrCode Stop;
    IrCode USB;
    IrCode Volume_Down;
    IrCode Volume_Up;
    IrCode Yellow;

    public IrCode getOne() {
        return One;
    }

    public void setOne(IrCode one) {
        One = one;
    }

    public IrCode getTwo() {
        return Two;
    }

    public void setTwo(IrCode two) {
        Two = two;
    }

    public IrCode getExit() {
        return exit;
    }

    public void setExit(IrCode exit) {
        this.exit = exit;
    }

    public IrCode getFast_Forward() {
        return Fast_Forward;
    }

    public void setFast_Forward(IrCode fast_Forward) {
        Fast_Forward = fast_Forward;
    }

    public IrCode getFreeze() {
        return Freeze;
    }

    public void setFreeze(IrCode freeze) {
        Freeze = freeze;
    }

    public IrCode getGreen() {
        return Green;
    }

    public void setGreen(IrCode green) {
        Green = green;
    }

    public IrCode getInput() {
        return Input;
    }

    public void setInput(IrCode input) {
        Input = input;
    }

    public IrCode getLast() {
        return Last;
    }

    public void setLast(IrCode last) {
        Last = last;
    }

    public IrCode getMenu() {
        return Menu;
    }

    public void setMenu(IrCode menu) {
        Menu = menu;
    }

    public IrCode getMute() {
        return Mute;
    }

    public void setMute(IrCode mute) {
        Mute = mute;
    }

    public IrCode getNavigate_Down() {
        return Navigate_Down;
    }

    public void setNavigate_Down(IrCode navigate_Down) {
        Navigate_Down = navigate_Down;
    }

    public IrCode getNavigate_Left() {
        return Navigate_Left;
    }

    public void setNavigate_Left(IrCode navigate_Left) {
        Navigate_Left = navigate_Left;
    }

    public IrCode getNavigate_Right() {
        return Navigate_Right;
    }

    public void setNavigate_Right(IrCode navigate_Right) {
        Navigate_Right = navigate_Right;
    }

    public IrCode getNavigate_Up() {
        return Navigate_Up;
    }

    public void setNavigate_Up(IrCode navigate_Up) {
        Navigate_Up = navigate_Up;
    }

    public IrCode getPlay() {
        return Play;
    }

    public void setPlay(IrCode play) {
        Play = play;
    }

    public IrCode getPause() {
        return Pause;
    }

    public void setPause(IrCode pause) {
        Pause = pause;
    }

    public IrCode getPower() {
        return Power;
    }

    public void setPower(IrCode power) {
        Power = power;
    }

    public IrCode getRADIO() {
        return RADIO;
    }

    public void setRADIO(IrCode RADIO) {
        this.RADIO = RADIO;
    }

    public IrCode getRed() {
        return Red;
    }

    public void setRed(IrCode red) {
        Red = red;
    }

    public IrCode getRewind() {
        return Rewind;
    }

    public void setRewind(IrCode rewind) {
        Rewind = rewind;
    }

    public IrCode getSelect() {
        return Select;
    }

    public void setSelect(IrCode select) {
        Select = select;
    }

    public IrCode getSleep() {
        return Sleep;
    }

    public void setSleep(IrCode sleep) {
        Sleep = sleep;
    }

    public IrCode getStop() {
        return Stop;
    }

    public void setStop(IrCode stop) {
        Stop = stop;
    }

    public IrCode getUSB() {
        return USB;
    }

    public void setUSB(IrCode USB) {
        this.USB = USB;
    }

    public IrCode getVolume_Down() {
        return Volume_Down;
    }

    public void setVolume_Down(IrCode volume_Down) {
        Volume_Down = volume_Down;
    }

    public IrCode getVolume_Up() {
        return Volume_Up;
    }

    public void setVolume_Up(IrCode volume_Up) {
        Volume_Up = volume_Up;
    }

    public IrCode getYellow() {
        return Yellow;
    }

    public void setYellow(IrCode yellow) {
        Yellow = yellow;
    }

    public IrCode getThree() {
        return Three;
    }

    public void setThree(IrCode three) {
        Three = three;
    }

    public IrCode getFour() {
        return Four;
    }

    public void setFour(IrCode four) {
        Four = four;
    }

    public IrCode getFive() {
        return Five;
    }

    public void setFive(IrCode five) {
        Five = five;
    }

    public IrCode getSix() {
        return Six;
    }

    public void setSix(IrCode six) {
        Six = six;
    }

    public IrCode getSeven() {
        return Seven;
    }

    public void setSeven(IrCode seven) {
        Seven = seven;
    }

    public IrCode getEight() {
        return Eight;
    }

    public void setEight(IrCode eight) {
        Eight = eight;
    }

    public IrCode getNine() {
        return Nine;
    }

    public void setNine(IrCode nine) {
        Nine = nine;
    }

    public IrCode getZero() {
        return Zero;
    }

    public void setZero(IrCode zero) {
        Zero = zero;
    }

    public IrCode getBack() {
        return Back;
    }

    public void setBack(IrCode back) {
        Back = back;
    }

    public IrCode getBlue() {
        return Blue;
    }

    public void setBlue(IrCode blue) {
        Blue = blue;
    }

    public IrCode getCS() {
        return CS;
    }

    public void setCS(IrCode CS) {
        this.CS = CS;
    }

    public IrCode getCard() {
        return Card;
    }

    public void setCard(IrCode card) {
        Card = card;
    }

    public IrCode getChannel_Down() {
        return Channel_Down;
    }

    public void setChannel_Down(IrCode channel_Down) {
        Channel_Down = channel_Down;
    }

    public IrCode getChannel_Up() {
        return Channel_Up;
    }

    public void setChannel_Up(IrCode channel_Up) {
        Channel_Up = channel_Up;
    }

    public IrCode getDIGITAL() {
        return DIGITAL;
    }

    public void setDIGITAL(IrCode DIGITAL) {
        this.DIGITAL = DIGITAL;
    }

    public IrCode getDTV() {
        return DTV;
    }

    public void setDTV(IrCode DTV) {
        this.DTV = DTV;
    }

    public IrCode getDelay() {
        return Delay;
    }

    public void setDelay(IrCode delay) {
        Delay = delay;
    }

    public IrCode getDisplay() {
        return Display;
    }

    public void setDisplay(IrCode display) {
        Display = display;
    }

    public IrCode getEnter() {
        return Enter;
    }

    public void setEnter(IrCode enter) {
        Enter = enter;
    }
}