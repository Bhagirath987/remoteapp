package com.minarath.tv.New;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.minarath.tv.R;

public class SubList_Adapter extends RecyclerView.Adapter<SubList_ViewHolder> {
    OnItemSelectionListener listener;
    private Context context;
    ArrayList<String> list = new ArrayList<>();

    public SubList_Adapter(Context context, ArrayList<String> list,
                           OnItemSelectionListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SubList_ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                 int viewType) {
        return new SubList_ViewHolder(LayoutInflater.from(context).inflate(R.layout.sublist_item,
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SubList_ViewHolder holder,
                                 final int position) {

        holder.txt_.setText(list.get(position));
        holder.txt_.setVisibility(View.VISIBLE);
        holder.txt_.setText(list.get(position));
        holder.R_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.setPos(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(ArrayList<String> list) {
        this.list = list;
        notifyDataSetChanged();
    }
}

class SubList_ViewHolder extends RecyclerView.ViewHolder {
    TextView txt_;
    LinearLayout R_layout;

    public SubList_ViewHolder(@NonNull View itemView) {
        super(itemView);
        txt_ = itemView.findViewById(R.id.txt_);
        R_layout = itemView.findViewById(R.id.R_layout);
    }
}