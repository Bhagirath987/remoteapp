package com.minarath.tv.New.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.minarath.tv.New.Database.DB_Helper;
import com.minarath.tv.New.OnItemSelectionListener;
import com.minarath.tv.R;
import com.minarath.tv.confielder_Main2Activity;
import com.minarath.tv.confielder_Recycler_Adapter;
import com.minarath.tv.databinding.ActivityDeviceListBinding;
import com.minarath.tv.utils.GridSpacingItemDecoration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Device_List_Activity extends AppCompatActivity {

    ActivityDeviceListBinding binding;
    List<String> list = new ArrayList<>();
    List<String> searchlist = new ArrayList<>();
    private RecyclerView rc_list;
    private confielder_Recycler_Adapter adapter;
    private Context context;
    private EditText etSearch;
    private ImageView toolLay;
    private TextView toolText;
    private DB_Helper db_helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_device__list_);

        etSearch = findViewById(R.id.etSearch);
        toolLay = findViewById(R.id.toolLay);
        toolText = findViewById(R.id.toolText);


        context = this;
        File database = Device_List_Activity.this.getDatabasePath(DB_Helper.DBNAME);
        db_helper = new DB_Helper(context);
        if (false == database.exists()) {
            db_helper.getReadableDatabase();
            if (db_helper.copyDataBase(Device_List_Activity.this)) {
                Log.e("Database Status", "Database Copied");
            } else {
                Log.e("Database Status", "Database Copy error");
            }
        }
        fillList();
        rc_list = findViewById(R.id.rc_list_device);
        rc_list.setHasFixedSize(true);
        rc_list.setLayoutManager(new GridLayoutManager(this, 2));


        int spanCount = 2; // 3 columns
        int spacing = 30; // 50px
        boolean includeEdge = true;
        rc_list.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));



        adapter = new confielder_Recycler_Adapter(this, (ArrayList<String>) list,
                new OnItemSelectionListener() {
                    @Override
                    public void setPos(int pos) {

                    }

                    @Override
                    public void setSelected(String isSelected) {
                        Intent intent = new Intent(Device_List_Activity.this, confielder_Main2Activity.class);
                        intent.putExtra("brand", isSelected);
                        startActivity(intent);
                    }
                });
        rc_list.setAdapter(adapter);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Search(etSearch.getText().toString());
            }
        });

        toolLay.setOnClickListener(v -> {
                    toolText.setVisibility(View.GONE);
                    toolLay.setVisibility(View.GONE);
                    binding.crdSearch.setVisibility(View.VISIBLE);

                }
        );

        binding.imgClose.setOnClickListener(v -> {
                    toolText.setVisibility(View.VISIBLE);
                    toolLay.setVisibility(View.VISIBLE);
                    binding.crdSearch.setVisibility(View.GONE);
                    binding.etSearch.setText("");

                }
        );
    }

    private void Search(String Enterd) {
        if (Enterd.equals("") && Enterd.equals(" ")) {
            adapter.setList((ArrayList<String>) list);
        } else {
            searchlist.clear();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).toLowerCase().contains(Enterd.toLowerCase())) {
                    searchlist.add(list.get(i));
                }
            }
            Collections.sort(searchlist);
            adapter.setList((ArrayList<String>) searchlist);
        }

    }


    public void GoBack(View view) {
        if (binding.crdSearch.getVisibility() == View.VISIBLE) {
            toolLay.setVisibility(View.VISIBLE);
            toolText.setVisibility(View.GONE);
            etSearch.setText("");
            binding.crdSearch.setVisibility(View.GONE);
            binding.toolText.setVisibility(View.VISIBLE);
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
         GoBack(null);
    }

    private void fillList() {
        list.clear();
        list = db_helper.getBrand();
        Log.e("List", ">>" + list.size());
        Collections.sort(list);
    }


}
