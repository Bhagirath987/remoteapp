package com.minarath.tv.New.Modal;

import java.io.Serializable;

public class IrCode implements Serializable {
    String Frequency;
    String IrCode;

    public IrCode(String frequency, String irCode) {
        Frequency = frequency;
        IrCode = irCode;
    }

    public String getFrequency() {
        return Frequency;
    }

    public void setFrequency(String frequency) {
        Frequency = frequency;
    }

    public String getIrCode() {
        return IrCode;
    }

    public void setIrCode(String irCode) {
        IrCode = irCode;
    }
}