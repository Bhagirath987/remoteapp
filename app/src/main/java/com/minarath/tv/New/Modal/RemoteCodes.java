package com.minarath.tv.New.Modal;

public class RemoteCodes {
    int id;
    String fragment;
    String btn_Fragment;
    String device;
    String brand;
    String frequency;
    String main_frame;

    public RemoteCodes(int id, String fragment, String btn_Fragment, String device, String brand,
                       String frequency, String main_frame) {
        this.id = id;
        this.fragment = fragment;
        this.btn_Fragment = btn_Fragment;
        this.device = device;
        this.brand = brand;
        this.frequency = frequency;
        this.main_frame = main_frame;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFragment() {
        return fragment;
    }

    public void setFragment(String fragment) {
        this.fragment = fragment;
    }

    public String getBtn_Fragment() {
        return btn_Fragment;
    }

    public void setBtn_Fragment(String btn_Fragment) {
        this.btn_Fragment = btn_Fragment;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getMain_frame() {
        return main_frame;
    }

    public void setMain_frame(String main_frame) {
        this.main_frame = main_frame;
    }
}
