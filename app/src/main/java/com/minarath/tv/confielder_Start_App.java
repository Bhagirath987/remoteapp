package com.minarath.tv;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import androidx.databinding.DataBindingUtil;

import com.minarath.tv.New.Activity.Device_List_Activity;
import com.minarath.tv.New.Activity.Fav_Activity;
import com.minarath.tv.New.Helper.Utils;
import com.minarath.tv.databinding.ConfielderActivityStartAppBinding;


public class confielder_Start_App extends Activity {


    ConfielderActivityStartAppBinding binding;
    ImageView start_btn, menu, tofav_btn;
    int w, h;
    PopupWindow mypopupWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding= DataBindingUtil.setContentView(this,R.layout.confielder_activity_start__app);
        Handler handler = new Handler(Looper.myLooper());
        handler.postDelayed(() -> {
            binding.imgLogo.setVisibility(View.VISIBLE);
            binding.startBtn.setVisibility(View.VISIBLE);
            binding.tofavBtn.setVisibility(View.VISIBLE);
            binding.textView3.setVisibility(View.VISIBLE);
            w = this.getResources().getDisplayMetrics().widthPixels;
            h = this.getResources().getDisplayMetrics().heightPixels;
            init();
        }, 1000);

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), Exit.class));
    }

    private void init() {
        start_btn = findViewById(R.id.start_btn);
        tofav_btn = findViewById(R.id.tofav_btn);
        menu = findViewById(R.id.menu);
    }

    public void Start(View view) {
        Intent intent = new Intent(confielder_Start_App.this, Device_List_Activity.class);
        startActivity(intent);
    }

    public void Fav(View view) {
        Intent intent = new Intent(confielder_Start_App.this, Fav_Activity.class);
        startActivity(intent);
    }

    public void Instruction(View view) {
//        Intent intent = new Intent(this, Instructions_Activity.class);
//        startActivity(intent);
    }


    public void Menu(View view) {
        mypopupWindow.showAtLocation(view, Gravity.TOP | Gravity.RIGHT, 0, 0);
    }
}